package life;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import life.civilization.Chinese;
import life.graphic.GameFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLSerializerTest {
	
	XMLSerializer xmlSerializer;

	@Before
	public void setUp() throws Exception {
		xmlSerializer = new XMLSerializer();
		World w = World.getInstance();	
		GameFrame frame = new GameFrame();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialize() throws SAXException, IOException, ParserConfigurationException {
		GameEntity entity = new Chinese(7, 5);
		
		String serialized = xmlSerializer.Serialize(entity);
		InputStream stream = new ByteArrayInputStream(serialized.getBytes(StandardCharsets.UTF_8));
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	    Document doc = documentBuilderFactory.newDocumentBuilder().parse(stream);
	    
	    Element gameOfLife = doc.getDocumentElement();
	    assertEquals("Incorrect root", "GameOfLife", gameOfLife.getNodeName());
	    
	    Element Common = (Element) gameOfLife.getElementsByTagName("Common").item(0);
	    
	    Node PosX = Common.getElementsByTagName("PosX").item(0);
	    assertEquals("Incorrect Posx", "7", PosX.getTextContent());

	    Node PosY = Common.getElementsByTagName("PosY").item(0);
	    assertEquals("Incorrect Posy", "5", PosY.getTextContent());
	    
	    Node Width = Common.getElementsByTagName("Width").item(0);
	    assertEquals("Incorrect width", "3", Width.getTextContent());

	    Node Height = Common.getElementsByTagName("Height").item(0);
	    assertEquals("Incorrect height", "3", Height.getTextContent());
	    
	    Node groupID = Common.getElementsByTagName("OriginalGroupID").item(0);
	    assertEquals("Incorrect group ID", "5", groupID.getTextContent());
	}

}
