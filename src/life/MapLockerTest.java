package life;

import static org.junit.Assert.*;

import java.awt.Point;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import life.civilization.Chinese;
import life.civilization.Civilization;
import life.civilization.Mongol;
import life.graphic.GameFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class MapLockerTest {
    
	MapLocker mapLocker;
	Point a;
	Point b;
	@Before
	public void setUp() throws Exception {
		mapLocker = new MapLocker(100, 100);
	}

	@After
	public void tearDown() throws Exception {
	}

	/*
	 * El test permite saber si el mapLocker esta haciendo lock a la parte del mapa que se supone que esta
	 * cerrando. 
	 */
	
	@Test
	public void testTakeRelease(){
		a = new Point(0,0);
		b = new Point(10,10);
		mapLocker.take(a, b);

		//Primera posicion tomada
		assertTrue(((ReentrantLock) (mapLocker.getLocks())[0][0]).isLocked());
		//Segunda posicion tomada
		assertTrue(((ReentrantLock) (mapLocker.getLocks())[5][5]).isLocked());
		//Tercera posicion tomada
		assertTrue(((ReentrantLock) (mapLocker.getLocks())[9][9]).isLocked());
		
		//Primer posicion NO tomada
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[11][10]).isLocked());		
		//Segunda posicion NO tomada
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[50][10]).isLocked());
		
		//Se hace release y se ve si realmente s liberaron
		mapLocker.release(a, b);
		
		//Primera posicion liberada
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[0][0]).isLocked());	
		//Segunda posicion liberada
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[5][5]).isLocked());
		//Tercera posicion tomada
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[9][9]).isLocked());
		
		//Posiciones que siguen sin ser tomada (No cambia)
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[11][10]).isLocked());
		
		assertFalse(((ReentrantLock) (mapLocker.getLocks())[50][10]).isLocked());
	}
}
