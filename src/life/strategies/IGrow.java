package life.strategies;

import life.civilization.Civilization;

public interface IGrow {
	public void go(Civilization c);
	public void growPopulation(Civilization c);

}
