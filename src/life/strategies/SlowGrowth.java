package life.strategies;

import life.civilization.Civilization;

public class SlowGrowth implements IGrow{

	@Override
	public void go(Civilization c) {
		growPopulation(c);
	}

	@Override
	public void growPopulation(Civilization c) {
		int newPop = (int) (c.getPopulation()*1.2);
		c.setPopulation(newPop);		
	}

}
