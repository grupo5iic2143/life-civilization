package life.strategies;

import life.civilization.Civilization;

public class StrongDefense implements IDefend{

	@Override
	public void go(Civilization c) {
		heal(c);
	}

	@Override
	public void heal(Civilization c) {
		int newPopu = (int) (c.getPopulation()*1.1);
		c.setPopulation(newPopu);		
	}
	
}
