package life.strategies;

import life.civilization.Civilization;

public interface IDie {
public void go(Civilization c);
}
