package life.strategies;

import life.civilization.Civilization;

public class StayInMapDeath implements IDie{

	@Override
	public void go(Civilization c) {
		c.Kill();
		c.transformToResource();
	}

}
