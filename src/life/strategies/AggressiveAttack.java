package life.strategies;

import life.civilization.Civilization;

public class AggressiveAttack implements IAttack{

	@Override
	public void go(Civilization c) {
		doDamage(c);
		changeSize(c);
	}

	@Override
	public void doDamage(Civilization c) {
		int newPopu = (int) (c.getPopulation()*0.5);
		c.setPopulation(newPopu);
	}

	@Override
	public void changeSize(Civilization c) {
		// TODO Auto-generated method stub
		
	}	

}
