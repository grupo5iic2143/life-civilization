package life.strategies;

import life.civilization.Civilization;

public class NormalDefense implements IDefend{

	@Override
	public void go(Civilization c) {
		heal(c);
	}
	@Override
	public void heal(Civilization c) {
		int newPopu = (int) (c.getPopulation()*1.05);
		c.setPopulation(newPopu);	
	}

}
