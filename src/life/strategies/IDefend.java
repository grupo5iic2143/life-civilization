package life.strategies;

import life.civilization.Civilization;

public interface IDefend {
	public void go(Civilization c);
	public void heal(Civilization c);
}
