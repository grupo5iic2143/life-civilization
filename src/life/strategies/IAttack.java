package life.strategies;

import life.civilization.Civilization;

public interface IAttack {
	public void go(Civilization c);
	public void doDamage(Civilization c);
	public void changeSize(Civilization c);

}
