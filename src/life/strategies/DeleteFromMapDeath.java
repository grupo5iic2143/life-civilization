package life.strategies;

import life.civilization.Civilization;

public class DeleteFromMapDeath implements IDie{

	@Override
	public void go(Civilization c) {
		c.Kill();
	}

}
