package life.strategies;

import life.civilization.Civilization;

public class FastGrowth implements IGrow{

	@Override
	public void go(Civilization c) {
		growPopulation(c);		
	}

	@Override
	public void growPopulation(Civilization c) {
		int newPop = (int) (c.getPopulation()*1.8);
		c.setPopulation(newPop);
	}

}
