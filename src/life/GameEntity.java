package life;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import life.civilization.*;

import life.civilization.Civilization;


public class GameEntity implements Runnable{
	private Point position; // Posicion en el mapa
	private int[][] form; //Mapa de ceros y unos indicando la forma.
	private int[] width_height;
	private Type type;
	private String otherWorldInfo = null;
	private String originalGroupID = null;
	
	/**
	 * Representa el color de la entidad en el mapa
	 */
	private Color color;
	/**
	 * Determina si la entidad está viva o muerta
	 */
	private boolean alive;
	/**
	 * Civilizaciones proximas a la civilizacion a las que puede atacar
	 */
	private List<GameEntity> attackCiv;
	@Override
	public void run() {

	}

	public GameEntity() {
		// TODO: cambiar esto, solucion parche
		width_height = new int[]{3,3};
		this.attackCiv = new ArrayList<GameEntity>();
		this.alive = true;
	}
	public void setAlive(boolean alive){
	this.alive = false;
	}
	public Point getPosition() {
		return position;
	}
	public void setPosition(Point position) {
		if(alive == true){
		this.position = position;
		}
	}
	public int[][] getForm() {
		return form;
	}
	public void setForm(int[][] dimensions) {
		if(alive == true){
		this.form = dimensions;
		width_height[0] = dimensions.length;
		width_height[1] = dimensions[0].length;
		}
	}

	public void setWidth_height(int x, int y)
	{
		if(alive == true){
		if(width_height==null)
			width_height = new int[2];

		
		width_height[0] = x;
		width_height[1] = y;
		}
	}

	public int[] getWidth_height()
	{
		return width_height;
	}
	public List<GameEntity> getAttackCiv()
	{
		
		return this.attackCiv;
	}

	public void addCiv(GameEntity game_entity)
	{	
		this.attackCiv.add(game_entity);
	}
	public void Kill(){
		
		MapLocker mapLocker = World.getInstance().getMapLocker();
		Point [] area = mapLocker.entityArea(this);
		mapLocker.take(area[0], area[1]);
		try{
		this.alive = false;		
		World.getInstance().getCivilizations().remove(this);	
		}
		finally{
		mapLocker.release(area[0], area[1]);
		}
	}
	public boolean isAlive(){
		return this.alive;
	}
	public Color getEntityColor(){
		return this.color;
	}
	public Type getType(){
		return type;
	}
	public void setColor(Color c){
		if(alive == true){
		this.color = c;
		}
	}
	
	public void setOtherWorldInfo(String s) {
		otherWorldInfo = s;
	}
	
	public String getOtherWorldInfo() {
		return otherWorldInfo;
	}
	
	public void setOriginalGroupID(String s) {
		originalGroupID = s;
	}
	
	public String getOriginalGroupID() {
		return originalGroupID;
	}
	

}
