package life.civilization;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.List;

import life.GameEntity;
import life.MapLocker;
import life.SpaceManager;
import life.World;
import life.XMLSerializer;
import life.graphic.GameFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CivNegotiationTest {

	Move moveManager;
	World world;
	Civilization gameEntity1;
	Civilization gameEntity2;
	SpaceManager spaceManager;
	List<GameEntity> gameEntities;
	GameFrame frame;
	
	@Before
	public void setUp() throws Exception {
		World w = World.getInstance();	
		w.setInitialCivilizationsAndResources(0, 0);
		w.setMap(new int[100][100]);
		w.setMapLocker(new MapLocker(100, 100));
		frame = new GameFrame();
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testCivNegotiation() throws InterruptedException{
		gameEntity1 = new Egypcian(1,1);
		gameEntity2 = new Egypcian(4,4);
		gameEntity1.setMoverange(0);
		gameEntity2.setMoverange(0);
		
		World.getInstance().addCivilizations(gameEntity1);
		World.getInstance().addCivilizations(gameEntity2);
		(new Thread(gameEntity1)).start();
		(new Thread(gameEntity2)).start();
		boolean statement = false;
		for(int i=0; i<10000;i++){
			Thread.sleep(10);
			statement = World.getInstance().getCivilizations().contains(gameEntity1)&& World.getInstance().getCivilizations().contains(gameEntity2);
			assertTrue(statement);
			if(gameEntity1 != null)
				gameEntity1.setPopulation(7);
			if(gameEntity2 != null)
				gameEntity2.setPopulation(7);
		}
	}
	
}
