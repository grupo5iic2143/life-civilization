package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.strategies.DeleteFromMapDeath;
import life.strategies.FastGrowth;
import life.strategies.NormalAttack;
import life.strategies.WeakDefense;


public class Chinese extends Civilization{
	public Chinese (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.CHINESE);
		this.setPosition(p);
		this.setPopulation(10);
		this.attack = new NormalAttack();
		this.defense = new WeakDefense();
		this.death = new DeleteFromMapDeath();
		this.growth = new FastGrowth();
		this.setMoverange(2);
		this.setSwiftness(1500);
		this.setWidth_height(3, 3);
		this.setColor(new Color(0xFFFF00));
	}
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{1,1,1},
					   	       	  {1,0,1},
					   	       	  {1,0,1}};
			break;
		case 4:
			newForm = new int[][]{{1,1,1,1},
								  {1,0,0,1},
			   	               	  {1,0,0,1},
			   	               	  {1,0,0,1}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,1,1},
								  {1,1,1,1,1},
								  {1,0,0,0,1},
								  {1,0,0,0,1},
								  {1,0,0,0,1}};
		case 6:
			newForm = new int[][]{{1,1,1,1,1,1},
								  {1,1,1,1,1,1},
								  {1,1,0,0,1,1},
								  {1,1,0,0,1,1},
								  {1,1,0,0,1,1},
								  {1,1,0,0,1,1}};
		}
		this.setForm(newForm);
	}
	public void interactWith(Civilization c){ 
		Random r = new Random();
		int probability = r.nextInt(10);
		switch(c.getType()){
		case CHINESE: 
			if(probability < 5){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);			}
			break;
		case EGYPCIAN:
			if(probability < 5){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case GOTH:
			if(probability < 8){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case GREEK:
			if(probability < 4){
				this.Attack(c);
			}
			else if(probability < 6){
				this.uniteWith(c);
			}
			break;
		case JAPANESE:
			this.Attack(c);
			break;
		case MONGOL:
			this.Attack(c);
			break;
		case PERSIAN:
			this.Attack(c);
			break;
		case ROMAN:
			this.Attack(c);
			break;
		case VIKING:
			if(probability < 4){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		}
	}

}
