package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.strategies.AggressiveAttack;
import life.strategies.SlowGrowth;
import life.strategies.StayInMapDeath;
import life.strategies.WeakDefense;


public class Japanese extends Civilization{
	public Japanese (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.JAPANESE);
		this.setPosition(p);
		this.setPopulation(5);
		this.attack = new AggressiveAttack();
		this.defense = new WeakDefense();
		this.death = new StayInMapDeath();
		this.growth = new SlowGrowth();
		this.setMoverange(3);
		this.setSwiftness(1000);
		this.setWidth_height(3, 4);
		this.setColor(new Color(0xCCCCCC));
	}
	
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{0,0,1,1},
					   	       	  {0,0,0,1},
					   	       	  {1,1,1,1}};
			break;
		case 4:
			newForm = new int[][]{{0,0,1,1,1},
								  {0,0,0,0,1},
								  {0,0,0,0,1},
								  {1,1,1,1,1}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,1,1,1},
								  {1,1,1,1,1,1},
								  {0,0,0,0,0,1},
								  {0,0,0,1,1,1},
								  {0,0,0,1,1,1}};
		case 6:
			newForm = new int[][]{{1,1,1,1,1,1,1},
								  {1,1,1,1,1,1,1},
								  {0,0,0,0,0,1,1},
								  {0,0,0,0,0,1,1},
								  {0,0,0,1,1,1,1},
								  {0,0,0,1,1,1,1}};
		}
		this.setForm(newForm);
	}

	public void interactWith(Civilization c){ 
		Random r = new Random();
		int probability = r.nextInt(10);
		switch(c.getType()){
		case CHINESE: 
			this.Attack(c);
			break;
		case EGYPCIAN:
			if(probability < 2){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		case GOTH:
			if(probability < 2){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		case GREEK:
			if(probability < 2){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		case JAPANESE:
			if(probability < 5){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
			break;
		case MONGOL:
			this.Attack(c);
			break;
		case PERSIAN:
			if(probability < 8){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		case ROMAN:
			if(probability < 6){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		case VIKING:
			if(probability < 2){
				this.Attack(c);
			}
			else{
				this.conquest(c);
			}
			break;
		}
	}
}
