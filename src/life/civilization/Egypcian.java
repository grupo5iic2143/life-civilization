package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.strategies.PasiveAttack;
import life.strategies.SlowGrowth;
import life.strategies.StayInMapDeath;
import life.strategies.StrongDefense;


public class Egypcian extends Civilization{
	public Egypcian (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.EGYPCIAN);
		this.setPosition(p);
		this.setPopulation(7);
		this.attack = new PasiveAttack();
		this.defense = new StrongDefense();
		this.death = new StayInMapDeath();
		this.growth = new SlowGrowth();
		this.setMoverange(1);
		this.setSwiftness(1000);
		this.setWidth_height(3, 4);
		this.setColor(new Color(0xFFDB58));
	}
	
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{1,1,1,1},
					   	       	  {1,1,0,1},
					   	       	  {1,0,0,1}};
			break;
		case 4:
			newForm = new int[][]{{1,1,1,1,1},
								  {1,0,1,0,1},
								  {1,0,1,0,1},
								  {1,0,0,0,1}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,1,1,1},
								  {1,0,1,1,0,1},
								  {1,0,1,1,0,1},
								  {1,0,1,1,0,1},
								  {1,1,1,1,0,1}};
		case 6:
			newForm = new int[][]{{1,1,1,1,1,1,1},
								  {1,1,1,1,1,1,1},
								  {1,1,0,1,0,1,1},
								  {1,1,0,1,0,1,1},
								  {1,1,0,1,0,1,1},
								  {1,1,0,1,0,1,1}};
		}
		this.setForm(newForm);
	}
	
	public void interactWith(Civilization c){ 
		Random r = new Random();
		int probability = r.nextInt(10);
		switch(c.getType()){
		case CHINESE: 
			if(probability < 4){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case EGYPCIAN:
			this.uniteWith(c);
			break;
		case GOTH:
			if(probability < 5){
				this.conquest(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case GREEK:
			if(probability < 8){
				this.conquest(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case JAPANESE:
			if(probability <  5){
				this.uniteWith(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case MONGOL:
			this.Attack(c);
			break;
		case PERSIAN:
			this.Attack(c);
			break;
		case ROMAN:
			if(probability < 6){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
			break;
		case VIKING:
			if(probability < 5){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		}
	}
}
