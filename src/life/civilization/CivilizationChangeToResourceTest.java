package life.civilization;

import static org.junit.Assert.*;

import java.util.Random;

import life.World;

import org.junit.Before;
import org.junit.Test;

public class CivilizationChangeToResourceTest {
	Civilization civ;
	Civilization civ2;
	Civilization civ3;
	
	@Before
	public void setUp() throws Exception {
		World w = World.getInstance();
		w.setEntities(1);
		civ = getResourceCiv();
		w.addCivilizations(civ);
	}

	@Test
	public void test() {
		civ.transformToResource();
		boolean deadAndResource= !civ.isAlive() && World.getInstance().getCivilizations().size() == 3;
		assertTrue("Should be dead and list of size 2", deadAndResource);
	}
	
	public Civilization getResourceCiv(){
		Random r = new Random();
		int random = r.nextInt(3);
		Civilization newCiv = null;
		switch(random){
		case 0:
			newCiv = (new Egypcian(1,1));
		case 1:
			newCiv = (new Greek(1,1));
		case 2:
			newCiv = (new Japanese(1,1));
		}
		return newCiv;
	}
}
