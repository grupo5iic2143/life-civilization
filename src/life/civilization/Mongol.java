package life.civilization;
import java.awt.Color;
import java.awt.Point;

import life.strategies.AggressiveAttack;
import life.strategies.DeleteFromMapDeath;
import life.strategies.FastGrowth;
import life.strategies.WeakDefense;


public class Mongol extends Civilization{
	public Mongol (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.MONGOL);
		this.setPosition(p);
		this.setPopulation(5);
		this.attack = new AggressiveAttack();
		this.defense = new WeakDefense();
		this.death = new DeleteFromMapDeath();
		this.growth = new FastGrowth();
		this.setMoverange(3);
		this.setSwiftness(500);
		this.setWidth_height(4, 3);
		this.setColor(new Color(0x00FF00));
	}
	
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 4: 
			newForm = new int[][]{{1,1,1},
					   	       	  {1,0,0},
					   	       	  {1,0,0},
					   	          {1,1,1}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,1},
								  {1,0,0,0},
			   	               	  {1,1,0,0},
			   	               	  {1,0,0,0},
			   	               	  {1,1,1,1}};
			break;
		case 6:
			newForm = new int[][]{{1,1,1,1,1},
								  {1,0,0,0,0},
								  {1,1,1,0,0},
								  {1,1,1,0,0},
								  {1,0,0,0,0},
								  {1,1,1,1,1}};
		case 7:
			newForm = new int[][]{{1,1,1,1,1,1},
								  {1,1,1,1,1,1},
								  {1,1,0,0,0,0},
								  {1,1,1,1,0,0},
								  {1,1,0,0,0,0},
								  {1,1,1,1,1,1},
								  {1,1,1,1,1,1}};
		}
		this.setForm(newForm);
	}
	
	public void interactWith(Civilization c){ 
		this.Attack(c);
	}
}
