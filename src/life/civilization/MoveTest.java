package life.civilization;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.text.Position;

import life.GameEntity;
import life.MapLocker;
import life.SpaceManager;
import life.World;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/*
 * Este test evalua el funcionamiento de Move y su integracion con SpaceManager
 */
public class MoveTest {

	
	Move moveManager;
	World world;
	Civilization gameEntity1;
	Civilization gameEntity2;
	SpaceManager spaceManager;
	List<GameEntity> gameEntities;
	
	@Before
	public void setUp() throws Exception {
		spaceManager = new SpaceManager();
		moveManager = new Move();
		world = World.getInstance();	
		world.setMap(new int[100][100]);
	}

	@After
	public void tearDown() throws Exception {
	}


	/*
	 * Verifica si se estan efectivamente moviendo y si se evita su 
	 * movimiento en caso de que haya una colision
	 */

	@Test
	public void testMoveGo(){
		Point comparator;
		boolean statement;
		gameEntity1 = new Mongol(10,10);
		Point posicion1 = gameEntity1.getPosition();
		
		//Verificacion en todas las direcciones
		for(int i=0;i<4;i++){
			moveManager.go(gameEntity1, i, spaceManager, gameEntities);
			comparator = posicion1;

			switch(i){
			case 0: comparator.translate(0, -gameEntity1.getMoverange()); break;
			case 1: comparator.translate(-gameEntity1.getMoverange(),0); break;
			case 2: comparator.translate(0, gameEntity1.getMoverange()); break;
			case 3: comparator.translate(gameEntity1.getMoverange(),0); break;
			}
			statement = (comparator.x==gameEntity1.getPosition().x) && (comparator.y==gameEntity1.getPosition().y);
			assertTrue(statement);
		}
		
	}
}
