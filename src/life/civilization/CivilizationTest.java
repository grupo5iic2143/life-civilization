	package life.civilization;

import static org.junit.Assert.*;

import java.util.Random;

import life.MessageMediator;
import life.World;
import life.graphic.GameFrame;

import org.junit.Before;
import org.junit.Test;

public class CivilizationTest {
	Civilization civ1;
	Civilization civ2;
	@Before
	public void setUp() throws Exception {
			civ1 = switchCivs();
			civ2 = switchCivs();
			civ1.messageMediator = MessageMediator.getInstance();
			civ2.messageMediator = MessageMediator.getInstance();
			civ1.messageMediator.setGameframe(new GameFrame());
			civ2.messageMediator.setGameframe(new GameFrame());
			World w = World.getInstance();
			w.setEntities(2);
			w.addCivilizations(civ1);
			w.addCivilizations(civ2);
			
	}
	public Civilization switchCivs(){
		Civilization newCiv = null;
		Random r = new Random();
		int civ = r.nextInt(9);
		switch(civ) {
		case 0:
			newCiv = new Mongol(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 1:
			newCiv = new Roman(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 2:
			newCiv = new Greek(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 3:
			newCiv = new Persian(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 4:
			newCiv = new Egypcian(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 5:
			newCiv = new Viking(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 6:
			newCiv = new Chinese(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 7:
			newCiv = new Japanese(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 8:
			newCiv = new Goth(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		}
		return newCiv;
	}
	@Test
	public void testAttack1() {
		civ1.setPopulation(200);
		civ2.setPopulation(200);
		civ1.Attack(civ2);
		boolean populationDiminished = civ2.getPopulation() < 200;
		assertTrue("should be less than initial", populationDiminished);
	}
	@Test
	public void testAttack2() {
		civ1.setPopulation(200);
		civ2.setPopulation(200);
		civ2.Attack(civ1);
		boolean populationDiminished = civ1.getPopulation() < 200;
		assertTrue("should be less than initial", populationDiminished);
	}
	@Test
	public void testGrowth(){
		civ1.setPopulation(200);
		civ1.Grow();
		boolean populationGrow = civ1.getPopulation() > 200;
		assertTrue("should be more than initial", populationGrow);
	}
	@Test
	public void testMaximumGrowth(){
		civ1.setPopulation(10000);
		civ1.Grow();
		boolean maxGrowth = civ1.getPopulation() == 10000;
		assertTrue("should me 10000", maxGrowth);
	}
	@Test
	public void testConquest(){
		civ1.setPopulation(200);
		civ2.setPopulation(200);
		civ1.conquest(civ2);
		boolean civ1Alive = civ1.isAlive();
		boolean civ2Dead = !civ2.isAlive();
		boolean listSizeCheck = (World.getInstance().getCivilizations().size() == 1);
		assertTrue("civ1 should be alive", civ1Alive);
		assertTrue("civ2 should be dead", civ2Dead);
		assertTrue("World list should be of size 1", listSizeCheck);
	}
	@Test
	public void uniteWith(){
		civ1.uniteWith(civ2);
		boolean civ1Alive = civ1.isAlive();
		boolean civ2Dead = !civ2.isAlive();
		boolean listSizeCheck = (World.getInstance().getCivilizations().size() == 1);
		assertTrue("civ1 should be alive", civ1Alive);
		assertTrue("civ2 should be dead", civ2Dead);
		assertTrue("World list should be of size 1", listSizeCheck);
	}
	
}
