package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.strategies.AggressiveAttack;
import life.strategies.DeleteFromMapDeath;
import life.strategies.NormalGrowth;
import life.strategies.WeakDefense;


public class Viking extends Civilization{
	public Viking (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.VIKING);
		this.setPosition(p);
		this.setPopulation(5);
		this.attack = new AggressiveAttack();
		this.defense = new WeakDefense();
		this.death = new DeleteFromMapDeath();
		this.growth = new NormalGrowth();
		this.setMoverange(2);
		this.setSwiftness(500);
		this.setWidth_height(3, 3);
		this.setColor(new Color(0x0000FF));
	}

	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{1,1,0},
					   	       	  {0,0,1},
					   	       	  {1,1,0}};
			break;
		case 4:
			newForm = new int[][]{{1,1,1,0},
								  {0,0,0,1},
			   	               	  {0,0,0,1},
			   	               	  {1,1,1,0}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,0,0},
								  {0,0,0,1,0},
								  {0,0,0,0,1},
								  {0,0,0,1,0},
								  {1,1,1,0,0}};
		case 6:
			newForm = new int[][]{{1,1,1,1,0,0},
								  {0,0,1,1,1,0},
								  {0,0,0,0,1,1},
								  {0,0,0,1,1,1},
								  {0,0,1,1,1,0},
								  {1,1,1,1,0,0}};
		}
		this.setForm(newForm);
	}
	
	public void interactWith(Civilization c){ 
		Random r = new Random();
		int probability = r.nextInt(10);
		switch(c.getType()){
		case CHINESE: 
			if(probability < 8){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case EGYPCIAN:
			if(probability < 6){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case GOTH:
			if(probability < 8){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case GREEK:
			if(probability < 2){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case JAPANESE:
			if(probability < 6){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case MONGOL:
			this.Attack(c);
			break;
		case PERSIAN:
			this.Attack(c);
			break;
		case ROMAN:
			this.Attack(c);
			break;
		case VIKING:
			if(probability < 7){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
			break;
		}
	}
}
