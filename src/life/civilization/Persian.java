package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.strategies.AggressiveAttack;
import life.strategies.DeleteFromMapDeath;
import life.strategies.FastGrowth;
import life.strategies.WeakDefense;


public class Persian extends Civilization{
	public Persian (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.PERSIAN);
		this.setPosition(p);
		this.setPopulation(10);
		this.attack = new AggressiveAttack();
		this.defense = new WeakDefense();
		this.death = new DeleteFromMapDeath();
		this.growth = new FastGrowth();
		this.setMoverange(2);
		this.setSwiftness(1500);
		this.setWidth_height(3, 4);
		this.setColor(new Color(0xC54B8C));
	}
	
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{1,1,1,1},
					   	       	  {1,0,1,0},
					   	       	  {0,1,0,0}};
			break;
		case 4:
			newForm = new int[][]{{1,1,1,1,1},
								  {1,1,1,1,1},
								  {1,0,1,0,0},
								  {1,1,1,0,0}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,1,1,1},
								  {1,1,1,1,1,1},
								  {1,0,1,0,0,0},
								  {1,1,1,0,0,0},
								  {1,1,1,0,0,0}};
		case 6:
			newForm = new int[][]{{1,1,1,1,1,1,1},
								  {1,1,1,1,1,1,1},
								  {1,0,0,1,1,0,0},
								  {1,0,0,1,1,0,0},
								  {1,1,1,1,1,0,0},
								  {1,1,1,1,1,0,0}};
		}
		this.setForm(newForm);
	}

	public void interactWith(Civilization c){ 
		if(c.getType().equals(Type.PERSIAN)){
			Random r = new Random();
			int probability = r.nextInt(10);
			if(probability < 2){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
		}
		else{
			this.conquest(c);
		}
	}
}
