package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.List;
import java.util.Random;
import life.GameEntity;
import life.MapLocker;
import life.MessageMediator;
import life.World;
import life.resource.Resource;
import life.strategies.IAttack;
import life.strategies.IDefend;
import life.strategies.IDie;
import life.strategies.IGrow;


public abstract class Civilization extends GameEntity{
	IAttack attack;
	IGrow growth;
	IDefend defense;
	IDie death;
	MessageMediator messageMediator;
	/**
	 * poblacion de la entidad
	 */
	private int population;
	/**
	 * determina cuanto se mueve en cada iteracion de su thread
	 */
	private int moverange;
	/**
	 * determina la velocidad en toma de decisiones de la entidad (sleep del thread)
	 */
	private int swiftness;
	/**
	 * Determina el nivel en el cual se encuentra la civilización (se relaciona con el tamaño)
	 */
	private int level;
	private Type type;



	@Override
	public void run() {
		int direction;
		Random r = new Random();
		messageMediator = MessageMediator.getInstance();


		while(isAlive()){
			direction = r.nextInt(4);
			if(this.getPopulation() != 10000){
				if(probabilityOf(8)){
					this.moveAndInteract(direction);
				}
				else{
					this.Grow();
				}
			}
			else{
				this.moveAndInteract(direction);
			}
			//Termino el ciclo de la civilizacion
			try {
				Thread.sleep(this.swiftness);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}


	/**
	 * se le pasa como parametro la civilizacion que sera atacada
	 */
	public void Attack(Civilization c){
		if(isAlive()){
			messageMediator.setSimulation("La civilizacion " + this.getType() + " en (" + this.getPosition().x + "," + this.getPosition().y
					+ ") ataco a " + c.getType() + " en (" + c.getPosition().x + "," + c.getPosition().y
					+ ")");
			attack.go(c);
			this.changeColorWhileAttacking();
			if(c.isAlive()){
				c.Defend();
			}
		}
	}
	/**
	 * Permite que las civilizaciones crezcan tanto en población como en tamaño
	 */
	public void Grow(){
		if(isAlive()){
			growth.go(this);
		}
	}
	/**
	 * Después de cada ataque las civilizaciones se defienden.
	 * Dependiendo su población al final de la defensa estas mueren o disminuyen su tamaño
	 */
	public void Defend(){
		if(isAlive()){
			this.changeColorWhileAttacking();	
			messageMediator.setSimulation("La civilizacion " + this.getType() + " se defendio");
			defense.go(this);
			/*System.out.println("La civilizacion " + this.getType() + " se defendió");
		defense.go(this);*/
		}
	}
	public void Die(){
		synchronized (this) {
			//System.out.println("La civilizacion " + this.getType() + " ha muerto");
			death.go(this);
			messageMediator.setSimulation("La civilizacion " + this.getType() + " ha muerto");
			World.getInstance().sumOneToDeads();
		}
	}
	public int getSwiftness(){
		return this.swiftness;
	}

	public void setSwiftness(int swift){
		if(numberBetween(swift, 300, 2200)){
			this.swiftness = swift;
		}
		else if(swift < 300){
			this.swiftness = 300;
		}
		else if(swift > 2200){
			this.swiftness = 2200;
		}
	}
	/**
	 * Consume el recurso dado
	 * @param resource - recurso a consumir
	 */
	public void Consume(Resource resource) {
		if(isAlive()){
			Type type = resource.getType();
			int newSwiftness = 0;
			if(resource.Consume(1)){
				switch(type) {
				case FOOD:
					messageMediator.setSimulation("La civilizacion " + this.getType() + " ha consumido 1 unidad de FOOD");
					//System.out.println("La civilización " + this.getType() + " ha consumido 1 unidad de FOOD");
					newSwiftness = getSwiftness() - 200;
					break;
				case RIVER:
					messageMediator.setSimulation("La civilizacion " + this.getType() + " ha consumido 1 unidad de RIVER");
					//System.out.println("La civilizacion " + this.getType() + " ha consumido 1 unidad de RIVER");
					newSwiftness = getSwiftness() + 100;
					break;
				case RUINS:
					messageMediator.setSimulation("La civilizacion " + this.getType() + " ha consumido 1 unidad de RUINS");
					//System.out.println("La civilizacion " + this.getType() + " ha consumido 1 unidad de RUINS");
					newSwiftness = getSwiftness() - 250;
					break;
				}
				this.setSwiftness(newSwiftness);
				this.changeColorWhileConsuming();
			}
		}
	}
	/**
	 * 
	 * @param newSwiftness nueva rapidez de decisión de la civilización
	 * @param newMoverange nuevo rango de movimiento de la civilización
	 */

	public int getPopulation(){
		return this.population;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		if(isAlive()){
			this.type = type;
		}
	}
	public void setPopulation(int pop){
		if(isAlive()){
			if(pop >= 10000){
				this.population = 10000;			
			}
			else{
				this.population = pop;
			}
			analizeGrowth();
		}
	}
	public void analizeGrowth(){
		if(this.getPopulation() < 5){
			this.Die();
		}
		if(this.getPopulation() < 40 && this.getLevel() != 3){
			this.changeSizeTo(3);
		}
		if(numberBetween(this.getPopulation(),40,239) && this.getLevel() != 4){
			this.changeSizeTo(4);
		}
		if(numberBetween(this.getPopulation(),240,2499) && this.getLevel() != 5){
			this.changeSizeTo(5);
		}
		if(numberBetween(this.getPopulation(),2500,10000) && this.getLevel() != 6){
			this.changeSizeTo(6);
		}
	}
	private boolean numberBetween(int num, int min, int max) {
		return min <= num && num <= max;
	}
	public void setMoverange(int moverange){
		this.moverange = moverange;
	}

	public int getMoverange(){
		return moverange;
	}

	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		if(isAlive()){
			this.level = level;
		}

	}
	public void transformToResource(){
		Resource resource = new Resource(Type.RUINS, getLevel(), 1, this);
		World.getInstance().getCivilizations().add(resource);
		this.Kill();
		// TODO - A�adir el recurso y remover esta entidad del flujo del juego
	}

	/**
	 * El método se preocupa de la interacción con otra entidad al estar al lado en el mapa.
	 */
	public void interactWith(Civilization c){

	}
	public void conquest(Civilization c){
		if(isAlive()){
			this.Attack(c);
			if(c.getPopulation() < this.getPopulation()){
				this.joinCivs(c);
				messageMediator.setSimulation("La civilizacion " + this.getType() + " en (" + this.getPosition().x + "," + this.getPosition().y
						+ ") se unio con, " + c.getType() + " en (" + c.getPosition().x + "," + c.getPosition().y
						+ "), luego de una conquista");	
				World.getInstance().sumOneToConquest();;
			}
			else{
				messageMediator.setSimulation("La civilizacion " + this.getType() + " en (" + this.getPosition().x + "," + this.getPosition().y
						+ ") intento conquistar a, " + c.getType() + " en (" + c.getPosition().x + "," + c.getPosition().y
						+ "), pero fallo");		
			}
		}
	}
	/**
	 * Se preocupa de que la unión entre civilizaciones
	 * @param c civilizacion que se va a unir con la actual
	 */
	public void uniteWith(Civilization c){
		c.changeColorWhileUnion();
		this.changeColorWhileUnion();		
		this.joinCivs(c);
		if(World.getInstance().getCivilizations().contains(this)){
		messageMediator.setSimulation("La civilizacion " + this.getType() + " en (" + this.getPosition().x + "," + this.getPosition().y
				+ ") se unio con, " + c.getType() + " en (" + c.getPosition().x + "," + c.getPosition().y
				+ "), luego de una negociacion politica");	
		World.getInstance().sumOneToFusions();
		}
	}
	public synchronized void joinCivs(Civilization c){
		this.setPopulation(this.getPopulation() + c.getPopulation());
		if(this.getMoverange() < c.getMoverange()){ //se quedan con el mejor moverange
			this.setMoverange(c.getMoverange());
		}

		synchronized(World.getInstance().getCivilizations()){
			if(World.getInstance().getCivilizations().contains(this)){
				c.Kill();
				if(this.getLevel() < 6){
					this.incrementSize();
				}
			}
		
		}


		
		

	}
	/**
	 * Se preocupa de hacer que las civilizaciones crezcan
	 */
	public void incrementSize(){
		if(isAlive()){
			if(this.getWidth_height()[0] == this.getWidth_height()[1] && this.getWidth_height()[0] < 6){ //cuadrada
				this.changeForm(this.getWidth_height()[0]);
				this.setLevel(this.getWidth_height()[0]);
			}
			else if(this.getWidth_height()[0] < this.getWidth_height()[1] && this.getWidth_height()[1] < 7){ //rectangular alto mayor al ancho
				this.changeForm(this.getWidth_height()[0]);
				this.setLevel(this.getWidth_height()[0]);
			}
			else if(this.getWidth_height()[0] > this.getWidth_height()[1] && this.getWidth_height()[0] < 7){ //rectangular alto menor al ancho
				this.changeForm(this.getWidth_height()[0]);
				this.setLevel(this.getWidth_height()[0] - 1);
			}
		}
	}
	/**
	 * 
	 * @param level establece el nivel al cual se cambia la civilizacion 3, 4, 5 o 6 (3x3,4x4,5x5,6x6/3x4,4x5,5x6,6x7/4x3,5x4,6x5,7x6)
	 */
	public void changeSizeTo(int level){
		if(!this.getType().equals(Type.MONGOL)){
			this.changeForm(level);
		}
		else {
			this.changeForm(level + 1);
		}
		this.setLevel(level);

	}
	/**
	 * Cambia la forma de la civilización cuando cambia su tamaño
	 * @param width es el nuevo ancho que tiene la civilización
	 */
	public void changeForm(int width){

	}
	public void moveAndInteract(int direction){

		/**
		 * Ataca o usa recurso luego de moverse
		 * La civilizacion ataca o consume recurso aleatoriamente, luego resetea su lista de civilizaciones/recursos
		 * con los que tiene contacto
		 */
		MapLocker mapLocker = World.getInstance().getMapLocker();
		Point [] area = mapLocker.entityArea(this);
		mapLocker.take(area[0], area[1]);
		Random r = new Random();
		try {
			Move movement = new Move();
			movement.go(this, direction, World.getInstance().getSpaceManager(), World.getInstance().getCivilizations());
			//System.out.println("Tamaño del arreglo: " + this.getAttackCiv().size());
		}
		finally {
			mapLocker.release(area[0], area[1]);
		}
		if(this.getAttackCiv().size() > 0){
			int target = r.nextInt(this.getAttackCiv().size());
			if(this.getAttackCiv().get(target) instanceof Civilization){
				this.interactWith((Civilization)this.getAttackCiv().get(target));
			}
			if(this.getAttackCiv().get(target) instanceof Resource){
				this.Consume((Resource)this.getAttackCiv().get(target));
			}
			this.getAttackCiv().clear();
		}

	}
	/**
	 * Hace que las entidades cambien de color al interactuar
	 * @param numberOfColorChange cantidad de veces que tiene que cambiar de color
	 */
	public void changeColorWhileAttacking(){
		Color realCivilizationColor = this.getEntityColor();
		if(isAlive()){
			int numberOfColorChange = 8;
			try {
				for(int i = 0; i < numberOfColorChange; i++){
					synchronized(this){
						Thread.sleep(125);
						this.setColor(new Color(000000));
						Thread.sleep(125);
						this.setColor(realCivilizationColor);
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				this.setColor(realCivilizationColor);
			}
		}
	}

	public void changeColorWhileConsuming(){
		Color realCivilizationColor = this.getEntityColor();
		if(isAlive()){
			int numberOfColorChange = 4;
			try {
				for(int i = 0; i < numberOfColorChange; i++){
					synchronized(this){
						Thread.sleep(125);
						this.setColor(new Color(0x0055FF));
						Thread.sleep(125);
						this.setColor(realCivilizationColor);
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				this.setColor(realCivilizationColor);
			}
		}
	}
	public void changeColorWhilePeace(){
		Color realCivilizationColor = this.getEntityColor();
		if(isAlive()){
			try {
				synchronized(this){
					this.setColor(new Color(0xFFFFFF));
					Thread.sleep(2000);
					this.setColor(realCivilizationColor);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				this.setColor(realCivilizationColor);
			}
		}
	}
	public void changeColorWhileUnion(){
		Color realCivilizationColor = this.getEntityColor();
		if(isAlive()){
			int numberOfColorChange = 8;
			try {
				for(int i = 0; i < numberOfColorChange; i++){
					synchronized(this){
						Thread.sleep(125);
						this.setColor(new Color(0xFFFFFF));
						Thread.sleep(125);
						this.setColor(realCivilizationColor);
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				this.setColor(realCivilizationColor);
			}
		}
	}
	public void setWidth_height(int width, int height){
		super.setWidth_height(width, height);
		this.changeForm(width);
	}
	/**
	 * 
	 * @param probability probabilidad en %/100
	 * @return si cumple con la probabilidad o no
	 */
	public boolean probabilityOf(int probability){
		Random r = new Random();
		int randomNumber = r.nextInt(9);
		return randomNumber < probability;
	}

}
