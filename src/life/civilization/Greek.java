package life.civilization;
import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.strategies.NormalDefense;
import life.strategies.PasiveAttack;
import life.strategies.SlowGrowth;
import life.strategies.StayInMapDeath;


public class Greek extends Civilization{
	public Greek (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.GREEK);
		this.setPosition(p);
		this.setPopulation(7);
		this.attack = new PasiveAttack();
		this.defense = new NormalDefense();
		this.death = new StayInMapDeath();
		this.growth = new SlowGrowth();
		this.setMoverange(2);
		this.setSwiftness(1000);
		this.setWidth_height(3, 3);
		this.setColor(new Color(0xCCCC00));
	}
	
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{1,0,0},
					   	       	  {0,1,1},
					   	       	  {1,0,0}};
			break;
		case 4:
			newForm = new int[][]{{1,0,0,0},
								  {0,1,1,1},
			   	               	  {0,1,1,1},
			   	               	  {1,0,0,0}};
			break;
		case 5:
			newForm = new int[][]{{1,0,0,0,0},
								  {0,1,0,0,0},
								  {1,1,1,1,1},
								  {0,1,0,0,0},
								  {1,0,0,0,0}};
		case 6:
			newForm = new int[][]{{1,0,0,0,0,0},
								  {0,1,1,0,0,0},
								  {1,1,1,1,1,1},
								  {1,1,1,1,1,1},
								  {0,1,1,0,0,0},
								  {1,0,0,0,0,0}};
		}
		this.setForm(newForm);
	}
	public void interactWith(Civilization c){
		switch(c.getType()){
		case CHINESE: 
			if(this.probabilityOf(2)){
				this.uniteWith(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		case EGYPCIAN:
			if(this.probabilityOf(9)){
				this.Attack(c);
			}
			break;
		case GOTH:
			if(this.probabilityOf(5)){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
			break;
		case GREEK:
			if(this.probabilityOf(4)){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
			break;
		case JAPANESE:
			if(this.probabilityOf(3)){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}
			break;
		case MONGOL:
		case PERSIAN:
			this.Attack(c);
			break;
		case ROMAN:
			if(this.probabilityOf(5)){
				this.Attack(c);
			}
			else{
				this.uniteWith(c);
			}			
			break;
		case VIKING:
			if(this.probabilityOf(7)){
				this.Attack(c);
			}
			else{
				this.changeColorWhilePeace();
			}
			break;
		}
	}
}
