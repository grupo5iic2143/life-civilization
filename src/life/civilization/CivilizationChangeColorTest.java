package life.civilization;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class CivilizationChangeColorTest {
	Civilization civ;
	@Before
	public void setUp() throws Exception {
		civ = switchCivs();
	}
	public Civilization switchCivs(){
		Civilization newCiv = null;
		Random r = new Random();
		int civ = r.nextInt(9);
		switch(civ) {
		case 0:
			newCiv = new Mongol(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 1:
			newCiv = new Roman(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 2:
			newCiv = new Greek(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 3:
			newCiv = new Persian(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 4:
			newCiv = new Egypcian(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 5:
			newCiv = new Viking(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 6:
			newCiv = new Chinese(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 7:
			newCiv = new Japanese(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		case 8:
			newCiv = new Goth(1,1); //no nos importan las ubicaciones, simularemos el ataque directamente
			break;
		}
		return newCiv;
	}
	@Test
	public void testChangeColorWhileInteracting() {
		Color civ1Color = civ.getEntityColor();
		civ.changeColorWhileAttacking();
		boolean TestForCiv = civ.getEntityColor() == civ1Color;
		assertTrue("Should be same as initial", TestForCiv);
	}

}
