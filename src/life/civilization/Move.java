package life.civilization;
import java.awt.Point;
import java.util.List;

import life.GameEntity;
import life.SpaceManager;
import life.World;


/**
 * 
 * @param c Civilización que se mueve
 * @param d Dirección en la que se mueve
 * @param spacemanager Clase que maneja los espacios del mapa
 * @param Entities Lista de todas las entidades
 *
 */
public class Move {
	public void go(Civilization c, int d, SpaceManager spacemanager, List<? extends GameEntity> Entities){
	 
	
		Point newPos = new Point(c.getPosition());	
		//Translate aumenta la posicion en deltaX y deltaY
		int xBorder = 0;
		int yBorder = 0;
		int xBorderMap = (World.getInstance().getMap()).length;
		int yBorderMap = (World.getInstance().getMap()).length;
		boolean traspaso = false;
		/*
		 * Evita traspasos de la frontera del mapa. xBorder e yBorder son los valores entregados
		 * al mover la civilizacion. Los otros dos valores son el borde del mapa
		 */
		String direction = "";
		switch (d){
		
		case 0://arriba
			newPos.translate(0, -c.getMoverange());		
			direction = "arriba";
			xBorder = newPos.x;
			yBorder = newPos.y;
			break;
		case 1://izquierda
			newPos.translate(-c.getMoverange(), 0);
			direction = "la izquierda";
			xBorder = newPos.x;
			yBorder = newPos.y;
			break;
		case 2://abajo
			newPos.translate(0, c.getMoverange());
			direction = "abajo";
			xBorder = newPos.x;
			yBorder = newPos.y;

			break;
		case 3://derecha
			newPos.translate(c.getMoverange(), 0);
			direction = "la derecha";
			xBorder = newPos.x;
			yBorder = newPos.y;
			break;
		}
		traspaso = (xBorder >= 0) && (yBorder >= 0) && (xBorder <= xBorderMap) && (yBorder <= yBorderMap);
		if( spacemanager.verifySpaceEntity(c, newPos,c.getWidth_height(),Entities) && traspaso)
		{
			//System.out.println("La posicion actual es: "+c.getPosition().x+" , "+ c.getPosition().y);
			c.setPosition(newPos);
			//System.out.println(c.getType() +" se ha movido hacia " + direction + ". Posicion actual: x " + newPos.x + " y: "+newPos.y);
		}
		else {
			//System.out.println(c.getType() + " en " + c.getPosition().x+","+c.getPosition().y + " no se pudo mover hacia " + direction);
		}
	}
	 
}
