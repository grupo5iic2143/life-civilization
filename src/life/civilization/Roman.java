package life.civilization;
import java.awt.Color;
import java.awt.Point;

import life.strategies.DeleteFromMapDeath;
import life.strategies.FastGrowth;
import life.strategies.NormalAttack;
import life.strategies.StrongDefense;

public class Roman extends Civilization{
	
	public Roman (int x, int y){
		Point p = new Point(x,y);
		this.setType(Type.ROMAN);
		this.setPosition(p);
		this.setPopulation(7);
		this.attack = new NormalAttack();
		this.defense = new StrongDefense();
		this.death = new DeleteFromMapDeath();
		this.growth = new FastGrowth();
		this.setMoverange(1);
		this.setSwiftness(1000);
		this.setWidth_height(3, 4);
		this.setColor(new Color(0xFF0000));
	}
	
	public void changeForm(int width){
		int[][] newForm = null;
		switch(width){
		case 3: 
			newForm = new int[][]{{1,1,1,1},
					   	       	  {1,0,1,0},
					   	       	  {1,1,0,1}};
			break;
		case 4:
			newForm = new int[][]{{1,1,1,1,1},
								  {1,0,1,0,0},
								  {1,1,1,1,0},
								  {0,0,0,0,1}};
			break;
		case 5:
			newForm = new int[][]{{1,1,1,1,1,1},
								  {1,1,1,1,1,1},
								  {1,0,0,1,0,0},
								  {1,0,1,0,1,0},
								  {0,1,0,0,0,1}};
		case 6:
			newForm = new int[][]{{1,1,1,1,1,1,1},
								  {1,1,1,1,1,1,1},
								  {1,0,0,1,0,0,0},
								  {1,0,0,1,0,0,0},
								  {1,0,1,0,1,0,0},
								  {1,1,0,0,0,1,1}};
		}
		this.setForm(newForm);
	}
	
	public void interactWith(Civilization c){ 
		if(c.getType().equals(Type.ROMAN)){
			this.uniteWith(c);
		}
		else{
			this.conquest(c);
		}
	}

}
