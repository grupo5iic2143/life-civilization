package life;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import life.civilization.Civilization;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SpaceManagerTest {

	SpaceManager spaceManager;
	List<GameEntity> entities;
	GameEntity gameEntity1;
	GameEntity gameEntity2;
	GameEntity gameEntity3;

	@Before
	public void setUp() throws Exception {
		spaceManager = new SpaceManager();
		entities = new ArrayList<GameEntity>();

		// Agregamos entidades
		/*   012345
		 * 0 ##
		 * 1 ##
		 * 2 ##  ##
		 * 3     ##
		 * 4 ####
		 * 5 ####
		 */
		gameEntity1 = squareEntity(0, 0, 1, 2);
		gameEntity2 = squareEntity(4, 2, 5, 3);
		gameEntity3 = squareEntity(0, 4, 3, 5);
		entities.add(gameEntity1);
		entities.add(gameEntity2);
		entities.add(gameEntity3);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testVerifySpace() {
		// deberia estar vacio
		Point start = new Point(0, 3);
		int[] dimensions = new int[]{4, 1};
		boolean empty = spaceManager.verifySpace(start, dimensions, entities);
		assertTrue("should be empty", empty);
		
		// deberia estar lleno
		start = new Point(1, 1);
		dimensions = new int[]{4, 2};
		empty = spaceManager.verifySpace(start, dimensions, entities);
		assertFalse("should not be empty", empty);
	}

	@Test
	public void testVerifySpaceEntity() {
		// deberia no chocar
		Point start = new Point(0, 3);
		int[] dimensions = gameEntity3.getWidth_height();
		boolean success = spaceManager.verifySpaceEntity(gameEntity3, start, dimensions, entities);
		assertTrue("should not collide empty", success);
		
		// deberia chocar
		start = new Point(1, 2);
		dimensions = gameEntity2.getWidth_height();
		success = spaceManager.verifySpace(start, dimensions, entities);
		assertFalse("should collide", success);
	}

	
	/*
	 * Crea una entidad cuadrada.
	 */
	private GameEntity squareEntity(int x1, int y1, int x2, int y2) {
		GameEntity entity = new GameEntity();
		entity.setPosition(new Point(x1, y1));
		entity.setForm(new int[x2 - x1 + 1][y2 - y1 + 1]);
		return entity;
	}
}
