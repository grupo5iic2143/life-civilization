package life.network;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class MigrationSender {

	public MigrationSender() { }

	/**
	 * Envia un string a otro computador.
	 * @param data
	 * @param address
	 * @param port
	 * @return
	 */
	public static boolean send(String data, InetAddress address, int port) {
		try (
			Socket socket = new Socket(address, port);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true)
        ) {
			out.println(data);
		}
		catch (IOException e) {
			return false;
		}
		return true;
	}
}
