package life.network;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import life.GameEntity;
import life.World;
import life.XMLSerializer;

/**
 * Clase encargada de realizar los procesos de migracion de entidades.
 */
public class MigrationManager {
	
	/**
	 * Lista de direcciones conocidas.
	 */
	List<IpPort> knownAddresses;
	
	/**
	 * Serializa las entidades.
	 */
	XMLSerializer xmlSerializer;

	public MigrationManager() {
		knownAddresses = new ArrayList<IpPort>();
		xmlSerializer = new XMLSerializer();
		
		// Creamos el receiver y empezamos a escuchar
		new MigrationReceiver(this);
	}
	
	/**
	 * Agrega una direccion a la lista de direcciones conocidas.
	 * @param ip direccion ip.
	 * @param port puerto.
	 */
	public void addKnownAddress(String name, InetAddress ip, int port) {
		knownAddresses.add(new IpPort(name, ip, port));
	}
	
	public void readPortsFrom(String path) {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(path));
			
			for (String key : prop.stringPropertyNames()) {
				String value = prop.getProperty(key);
				String[] parts = value.split(":");
				
				if (parts.length == 2) {
					parseAndAdd(key, parts[0], parts[1]);
				}
				else {
					System.err.println("ERROR: '" + key + "' no esta en el formato correcto.");
				}
			}
		}
		catch(IOException e) {
			System.err.println("ERROR: no se pudo leer '" + path + "'.");
		}
	}
	
	private void parseAndAdd(String name, String ip, String port) {
		InetAddress _ip;
		int _port;

		try {
			_ip = InetAddress.getByName(ip);
			_port = Integer.valueOf(port);
			addKnownAddress(name, _ip, _port);
		}
		catch (UnknownHostException e) {
			System.err.println("ERROR: error en la ip '" + ip + "'.");
		}
		catch (NumberFormatException e) {
			System.err.println("ERROR: error in port number '" + port + "'.");
		}
	}
	
	public List<IpPort> getKnownAddresses() {
		return knownAddresses;
	}
	
	/**
	 * Envia la entidad a la direccion indicada.
	 * @param entity
	 * @param destination
	 */
	public void emigrate(GameEntity entity, IpPort destination) {
		// TODO - llenar esto para que env�e la entidad
		if(entity!=null){
		entity.Kill();
		String serialized = xmlSerializer.Serialize(entity);
		
		boolean success = MigrationSender.send(serialized, destination.ip, destination.port);
		// TODO - Hacer algo si falla y success = false
		}
		
	}
	
	/**
	 * Agrega una entidad recibida al mundo.
	 * @param serialized
	 */
	public void inmigrate(String serialized) {
		xmlSerializer.Deserialize(serialized);
	}

	/**
	 * Representa un par ip-puerto.
	 */
	public class IpPort {
		public String name;
		public InetAddress ip;
		public int port;
		
		public IpPort(String name, InetAddress ip, int port) {
			this.name = name;
			this.ip = ip;
			this.port = port;
		}
		
		public String toString() {
			return name;
		}
	}
	
	public XMLSerializer getXMLSerializer(){
		return this.xmlSerializer;
	}
}
