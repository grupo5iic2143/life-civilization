package life.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class MigrationReceiver {
	
	public final int PORT = 4444;
	private ServerSocket server;
	private MigrationManager migrationManager;

	public MigrationReceiver(MigrationManager migrationManager) {
		this.migrationManager = migrationManager;

		try {
			server = new ServerSocket(PORT);
		}
		catch (IOException e) {
			System.err.println("ERROR: No se pudo iniciar el servidor.");
		}
		
		Thread t = new Thread( new Runnable() {
			@Override
			public void run() {
				while (true) {
					listen();
				}
			}
		});
		
		t.start();
	}
	
	public void listen() {
		String message = null;

		try (
			Socket client = server.accept();
			BufferedReader in = new BufferedReader(
				new InputStreamReader(client.getInputStream()));
		) {
			message = in.readLine();
		}
		catch (IOException e) { }
		
		if (message == null || message.isEmpty()) return;
		
        migrationManager.inmigrate(message);
	}

}
