package life.resource;

import static org.junit.Assert.*;

import java.awt.Point;

//import life.resource.Resource.Type;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ResourceTest {
	
	private Resource resource;

	@Before
	public void setUp() throws Exception {
		resource = new Resource(life.civilization.Type.FOOD, 4, 1, new Point(0, 0));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConsume() {
		int amount = 3;

		int oldQuantity = resource.getQuantity();
		boolean success = resource.Consume(amount);
		int newQuantity = resource.getQuantity();
		
		assertTrue(success);
		assertEquals(amount, oldQuantity - newQuantity);
	}
	
	@Test
	public void testRegeneration() {
		int regeneration = resource.getRegeneration();
		
		int oldQuantity = resource.getQuantity();
		resource.update();
		int newQuantity = resource.getQuantity();
		
		assertEquals(regeneration, newQuantity - oldQuantity);
	}

}
