package life.resource;

import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import life.GameEntity;
import life.World;
import life.civilization.Civilization;
import life.civilization.Type;

/**
 * Representa un recurso en el juego (ej. comida, r�o, etc.)
 * @author Matu
 */
public class Resource extends GameEntity{

	/**
	 * Tipo de recurso.
	 */
	/*public enum Type {
		FOOD, RIVER, RUINS
	}*/

	private int quantity;
	private final int regeneration;
	private final Type type;
	private final int[][] Form;

	public Type getType() { return type; }

	public Resource(Type type, int quantity, int regeneration, Point p) {
		this.type = type;
		this.quantity = quantity;
		this.regeneration = regeneration;
		this.Form = createFormAndColor();
		setForm(Form);
		setPosition(p);
	}
	public Resource(Type type, int quantity, int regeneration, Civilization c) {
		this.type = type;
		this.quantity = quantity;
		this.regeneration = regeneration;
		this.Form = setFormByCivilization(c);
		setForm(Form);
		setPosition(c.getPosition());
	}
	@Override
	public void run(){
		while(this.isAlive()){
			this.update();
			//Termino el ciclo del recurso
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * Consume la cantidad dada de este recurso.
	 * @param amount - cantidad a consumir
	 * @return si quedaba suficiente recurso
	 */
	public synchronized boolean Consume(int amount) {
		if(amount > quantity){
			return false;
		}
		quantity -= amount;
		if (quantity <= 0){
			World.getInstance().sumOneToResEaten();
			Kill();
		}
		return true;
	}
	public int[][] createFormAndColor(){
		int[][] form = null;
		if(this.type.equals(Type.FOOD)){
			form = getRandomFood();
		}
		if(this.type.equals(Type.RIVER)){
			form = getRandomRiver();
		}
		return form;
	}
	public int[][] getRandomRiver(){
		Random r = new Random();
		int riverType = r.nextInt(2);
		int [][] randomRiver = null;
		switch(riverType){
		case 0:
		randomRiver =  new int[][]{{0,1,0},
				{0,1,0},
				{0,1,0},
				{0,1,0},
				{0,1,0},
				{0,1,0}};
		setColor(new Color(0xB2FFFF));
		break;
		case 1:
		randomRiver =  new int[][]{{0,0,0,0,0,0},
							{1,1,1,1,1,1},
							{0,0,0,0,0,0}};
		setColor(new Color(0xB2FFFF));
		break;
		}
		return randomRiver;
	}
	public int[][] getRandomFood(){
		Random r = new Random();
		int[][] randomFood = null;
		int rand = r.nextInt(5);
		switch(rand){
		case 0: //Fruta cuadrada
			randomFood = new int[][]{{1,1,1,1},
					{1,1,1,1},
					{1,1,1,1},
					{1,1,1,1}};
			setColor(new Color(0xD9E542));
			break;
		case 1: //Manzana
			randomFood = new int[][]{{0,1,0,1,0},
					{1,1,1,1,1},
					{1,1,1,1,1},
					{1,1,1,1,1},
					{0,1,1,1,0}};
			setColor(new Color(0x00FF00));
			break;
		case 2: //Platano
			randomFood = new int[][]{{0,1,1},
					{1,1,0},
					{1,1,0},
					{1,1,0},
					{1,1,0},
					{0,1,1}};
			setColor(new Color(0xEDD051));
			break;
		case 3://Berries
			randomFood = new int[][]{{0,1,1,0},
					{1,1,1,1},
					{1,1,1,1},
					{0,1,1,0}};
			setColor(new Color(0xC54B8C));
			break;
		case 4://Sandía
			randomFood = new int[][]{{0,1,1,1,1,1,1,0},
					{1,1,1,1,1,1,1,1},
					{1,1,1,1,1,1,1,1},
					{0,1,1,1,1,1,1,0}};
			setColor(new Color(0x0E370C));
			break;
		}
		return randomFood;

	}
	public int[][] setFormByCivilization(Civilization c){
		int[][]newForm = null;
		if(c.getType().equals(life.civilization.Type.EGYPCIAN)){
			newForm = new int[][]{{0,0,1,0,0},
					{0,1,1,1,0},
					{1,1,1,1,1}};
			setColor(new Color(0xFFDB58));
		}
		else if((c.getType().equals(life.civilization.Type.GREEK))){
			newForm = new int[][]{{1,1,1,1,1},
					{1,0,1,0,1},
					{1,1,1,1,1}};
			setColor(new Color(0x7A7A7A));
		}
		else if((c.getType().equals(life.civilization.Type.JAPANESE))){
			newForm = new int[][]{{0,1,0,0,0},
					{1,1,1,1,1},
					{0,1,0,0,0}};
			setColor(new Color(0x7A7A7A));
		}
		return newForm;
	}
	public void update() {
		if(quantity < 5){
			quantity = quantity + regeneration;
		}
		if(quantity > 5){
			quantity = 5;
		}
	}
	public int getQuantity(){
		return this.quantity;
	}
	public int getRegeneration(){
		return this.regeneration;
	}
}
