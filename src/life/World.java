package life;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.awt.Point;

import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.TypeVisitor;

import life.civilization.Chinese;
import life.civilization.Civilization;
import life.civilization.Egypcian;
import life.civilization.Goth;
import life.civilization.Greek;
import life.civilization.Japanese;
import life.civilization.Mongol;
import life.civilization.Persian;
import life.civilization.Roman;
import life.civilization.Viking;
import life.network.MigrationManager;
import life.network.MigrationManager.IpPort;
import life.resource.Resource;


public class World {
	private final String ADDRESS_FILE = "addresses.properties";

	private List<GameEntity> entities;
	private SpaceManager spaceManager;
	private MigrationManager migrationManager;
	private int[][] map;
	private MapLocker mapLocker;
	private int humidity, temperature;
	private static World world= null;
	private int weather;
	private MessageMediator messageMediator;
	private int seconds;
	private int appearances;
	private int migrated;
	private int deads;
	private int fusions;
	private int conquest;
	private int civCreated;
	private int resCreated;
	private int receivedRes;
	private int receivedCiv;
	private int resEaten;

	private World(){
		spaceManager = new SpaceManager();
		migrationManager = new MigrationManager();
		migrationManager.readPortsFrom(ADDRESS_FILE);
		appearances = 0;
		migrated = 0;
		deads = 0;
		fusions = 0;
		civCreated = 0;
		resCreated = 0;
		receivedRes = 0;
		receivedCiv = 0;
		resEaten = 0;
		conquest = 0;
		
	}

	public static World getInstance(){
		if(world == null){
			world = new World();
		}
		return world;
	}
	public List<GameEntity> getCivilizations() {
		return entities;
	}
	public void setEntities(int n){
		entities = new ArrayList<>(n);
		mapLocker = new MapLocker(10 + 5*n, 10 + 5*n);
	}

	public void setInitialCivilizationsAndResources(int initialCivils, int initialResources){
		int n = initialCivils + initialResources;
		entities = new ArrayList<>(n);
		map = new int[10 + 5*n][10 + 5*n];
		mapLocker = new MapLocker(10 + 5*n, 10 + 5*n);
		CreateRandomCivilizations(initialCivils, n);
		CreateRandomResources(initialResources, n);
	}
	public void addCivilizations(Civilization c) {
		this.entities.add(c);

	}

	public void sumOneToCivCreated(){
		civCreated++;
	}
	public void sumOneToConquest(){
		conquest++;
	}
	
	public void sumOneToDeads(){
		deads++;
	}
	
	public void sumOneToFusions(){
		fusions++;
	}
	public void sumOneToReceivedRes(){
		receivedRes++;
	}
	public void sumOneToResEaten(){
		resEaten++;
	}
	public void sumOneToResCreated(){
		resCreated++;
	}

	public void sumOneToAppearences(){
		appearances++;
	}
	public int getCivCreated(){
		return civCreated;
	}
	public int getConquest(){
		return conquest;
	}
	public int getCivDeads(){
		return deads;
	}
	public int getFusions(){
		return fusions;
	}
	public int getAppearances(){
		return appearances;
	}
	public int getMigrated(){
		return migrated;
	}
	public int getResCreated(){
		return resCreated;
	}
	public int getReceiverCiv(){
		return receivedCiv;
	}
	public int getReceiverRes(){
		return receivedRes;
	}

	public int getResEaten(){
		return resEaten;
	}
	
	
	public MigrationManager getMigrationManager(){
		return this.migrationManager;
	}
		
	public MapLocker getMapLocker() {
		return mapLocker;
	}	
	
	public void setMapLocker(MapLocker _mapLocker){
		this.mapLocker = _mapLocker;
	}
	
	public int[][] getMap() {
		return map;
	}

	public void setMap(int[][] map) {
		this.map = map;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public SpaceManager getSpaceManager(){
		return this.spaceManager;
	}
	
	public void setWeather(int typeWeather){
		weather = typeWeather;
	}
	
	private void CreateRandomCivilizations(int numberOfCivilizations, int numberOfEntities) {
		for( int i = 0; i < numberOfCivilizations; i++ ) {			
			int civ = selectCiv();
			addCivilization(numberOfEntities, civ);
		}
	}
	
	public void CreateOneRandomCivilizations() {
		
		
		int civ = selectCiv();
		int n = getCivilizations().size();
		addCivilization(n, civ);
		
	}
	
	private int selectCiv(){
		int typeCiv = 0;
		double prob = 0;
		Random r = new Random();
		prob = r.nextDouble();
		if(weather == 0){			
			if(prob <= 0.35){
				typeCiv = 4;				
			}
			else if(prob > 0.35 && prob <= 0.7){
				typeCiv = 3;				
			}
			else{
				typeCiv = r.nextInt(9);				
			}				
		}
		
		else if(weather == 1){
			if(prob <= 0.25){
				typeCiv = 1;				
			}
			else if(prob > 0.25 && prob <= 0.5){
				typeCiv = 2;				
			}
			else if(prob > 0.5 && prob <= 0.75){
				typeCiv = 6;				
			}
			else{
				typeCiv = r.nextInt(9);				
			}	
		}
		
		else if(weather == 2){
			if(prob <= 0.35){
				typeCiv = 5;				
			}
			else if(prob > 0.35 && prob <= 0.7){
				typeCiv = 8;				
			}
			else{
				typeCiv = r.nextInt(9);				
			}	
		}
		
		else if(weather == 3){
			if(prob <= 0.35){
				typeCiv = 7;				
			}
			else if(prob > 0.35 && prob <= 0.7){
				typeCiv = 0;				
			}
			else{
				typeCiv = r.nextInt(9);				
			}	
		}	
		
		
		return typeCiv;		
	}
	
	private void CreateRandomResources(int numberOfResources, int numberOfEntities) {
		Random r = new Random();
		for( int i = 0; i < numberOfResources; i++ ) {

			int type = r.nextInt(6);
			addResource(numberOfEntities, type);
		}
	}
	
	public void setTime(int time) {
		seconds = time;
	}
	public int getTime() {
		return seconds;
	}
	
	private void addResource(int n, int res){
		Random r = new Random();
		int x = r.nextInt(10 + 5*n);
		int y = r.nextInt(10 + 5*n);
		Point p = new Point(x,y);
		Resource newResource;
		if(res < 5){
			newResource = new Resource(life.civilization.Type.FOOD, r.nextInt(5) + 1, r.nextInt(2), p);
			if(spaceManager.verifySpace(newResource.getPosition(), newResource.getWidth_height(), entities)){
				entities.add(newResource);
				(new Thread(newResource)).start();
			}
			else{
				addResource(n, res);
			}
		}
		else{
			newResource = new Resource(life.civilization.Type.RIVER, r.nextInt(5) + 1, r.nextInt(2), p);
			if(spaceManager.verifySpace(newResource.getPosition(), newResource.getWidth_height(), entities)){
				entities.add(newResource);
				(new Thread(newResource)).start();
			}
			else{
				addResource(n, res);
			}
		}		
	}

	public void addOneResource(Point point, int res){
		Random r = new Random();
		Point p = point;
		Resource newResource;
		
		switch(res){
		case 0:
			newResource = new Resource(life.civilization.Type.FOOD, r.nextInt(5) + 1, r.nextInt(2), p);
			if(spaceManager.verifySpace(newResource.getPosition(), newResource.getWidth_height(), entities)){
				entities.add(newResource);
				(new Thread(newResource)).start();
			}

			break;
		case 1:
			newResource = new Resource(life.civilization.Type.RIVER, r.nextInt(5) + 1, r.nextInt(2) + 1, p);
			if(spaceManager.verifySpace(newResource.getPosition(), newResource.getWidth_height(), entities)){
				entities.add(newResource);
				(new Thread(newResource)).start();
			}

			break;
		}
	}

	public void addOneResource(Point point, int res, String originalGroupID, String otherWorldInfo){
		Random r = new Random();
		Point p = point;
		Resource newResource;
		
		switch(res){
		case 0:
			newResource = new Resource(life.civilization.Type.FOOD, r.nextInt(5) + 1, r.nextInt(2), p);
			newResource.setOriginalGroupID(originalGroupID);
			newResource.setOtherWorldInfo(otherWorldInfo);
			if(spaceManager.verifySpace(newResource.getPosition(), newResource.getWidth_height(), entities)){
				entities.add(newResource);
				(new Thread(newResource)).start();
			}

			break;
		case 1:
			newResource = new Resource(life.civilization.Type.RIVER, r.nextInt(5) + 1, r.nextInt(2) + 1, p);
			newResource.setOriginalGroupID(originalGroupID);
			newResource.setOtherWorldInfo(otherWorldInfo);
			if(spaceManager.verifySpace(newResource.getPosition(), newResource.getWidth_height(), entities)){
				entities.add(newResource);
				(new Thread(newResource)).start();
			}

			break;
		}
	}

	private void addCivilization(int n, int civ){

		
		Random r = new Random();		
		int x = r.nextInt(10 + 5*n); //por cada civilizacion inicial se agregan 5 columnas y 5 filas a los 10x10 base
		int y = r.nextInt(10 + 5*n);
		int[] Weidth_Heigth= new int[2];
		Weidth_Heigth[0] = 3;
		Weidth_Heigth[1] = 3;//comienzan siendo de 3x3
		Point point = new Point(x,y);

		boolean verify = spaceManager.verifySpace(point, Weidth_Heigth, entities);
		civilizationAddition(verify, civ, x, y, n);
	}
	/*
	 * El else en este metodo envia un mensaje que diga que no se puede colocar en ese lugar la
	 * civilizacion
	 * Si civ es -1 se trata de una entidad de otro mundo, por definir que se hara con ella
	 */
	public void addOneCivilization(Point position, int[] dimensions, int groupId, int civ){
		int x = position.x;
		int y = position.y;
		if(civ != -1){
			boolean verify = spaceManager.verifySpace(position, dimensions, entities);
			civilizationAddition(verify, civ, x, y, 0);
		}
		
	}

	public void addOneCivilization(Point position, int[] dimensions, int groupId, int civ,
								   String originalGroupID, String otherWorldInfo){
		int x = position.x;
		int y = position.y;
		if(civ != -1){
			boolean verify = spaceManager.verifySpace(position, dimensions, entities);
			civilizationAddition(verify, civ, x, y, 0, originalGroupID, otherWorldInfo);
		}
		
	}

	public void civilizationAddition(boolean verificationOfSpace, int civ, int x, int y, int n){
		if (verificationOfSpace == true){
			switch(civ) {
			case 0:
				Civilization newCiv = new Mongol(x, y);
				entities.add(newCiv);
				
				(new Thread(newCiv)).start();
				break;
			case 1:
				Civilization newCiv1 = new Roman(x, y);
				entities.add(newCiv1);
				(new Thread(newCiv1)).start();
				break;
			case 2:
				Civilization newCiv2 = new Greek(x, y);
				entities.add(newCiv2);
				(new Thread(newCiv2)).start();
				break;
			case 3:
				Civilization newCiv3 = new Persian(x, y);
				entities.add(newCiv3);
				(new Thread(newCiv3)).start();
				break;
			case 4:
				Civilization newCiv4 = new Egypcian(x, y);
				entities.add(newCiv4);
				(new Thread(newCiv4)).start();
				break;
			case 5:
				Civilization newCiv5 = new Viking(x, y);
				entities.add(newCiv5);
				(new Thread(newCiv5)).start();
				break;
			case 6:
				Civilization newCiv6 = new Chinese(x, y);
				entities.add(newCiv6);
				(new Thread(newCiv6)).start();
				break;
			case 7:
				Civilization newCiv7 = new Japanese(x, y);
				entities.add(newCiv7);
				(new Thread(newCiv7)).start();
				break;
			case 8:
				Civilization newCiv8 = new Goth(x, y);
				entities.add(newCiv8);
				(new Thread(newCiv8)).start();
				break;
			}

		}

		else{
			addCivilization(n, civ);
		}
	}

	public void civilizationAddition(boolean verificationOfSpace, int civ, int x, int y, int n,
									 String originalGroupID, String otherWorldInfo){
		if (verificationOfSpace == true){
			switch(civ) {
			case 0:
				Civilization newCiv = new Mongol(x, y);
				newCiv.setOriginalGroupID(originalGroupID);
				newCiv.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv);
				(new Thread(newCiv)).start();
				break;
			case 1:
				Civilization newCiv1 = new Roman(x, y);
				newCiv1.setOriginalGroupID(originalGroupID);
				newCiv1.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv1);
				(new Thread(newCiv1)).start();
				break;
			case 2:
				Civilization newCiv2 = new Greek(x, y);
				newCiv2.setOriginalGroupID(originalGroupID);
				newCiv2.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv2);
				(new Thread(newCiv2)).start();
				break;
			case 3:
				Civilization newCiv3 = new Persian(x, y);
				newCiv3.setOriginalGroupID(originalGroupID);
				newCiv3.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv3);
				(new Thread(newCiv3)).start();
				break;
			case 4:
				Civilization newCiv4 = new Egypcian(x, y);
				newCiv4.setOriginalGroupID(originalGroupID);
				newCiv4.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv4);
				(new Thread(newCiv4)).start();
				break;
			case 5:
				Civilization newCiv5 = new Viking(x, y);
				newCiv5.setOriginalGroupID(originalGroupID);
				newCiv5.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv5);
				(new Thread(newCiv5)).start();
				break;
			case 6:
				Civilization newCiv6 = new Chinese(x, y);
				newCiv6.setOriginalGroupID(originalGroupID);
				newCiv6.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv6);
				(new Thread(newCiv6)).start();
				break;
			case 7:
				Civilization newCiv7 = new Japanese(x, y);
				newCiv7.setOriginalGroupID(originalGroupID);
				newCiv7.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv7);
				(new Thread(newCiv7)).start();
				break;
			case 8:
				Civilization newCiv8 = new Goth(x, y);
				newCiv8.setOriginalGroupID(originalGroupID);
				newCiv8.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv8);
				(new Thread(newCiv8)).start();
				break;
			}

		}

		else{
			addCivilization(n, civ);
		}
	}

	public void addOneCivilizationMigrate(Point position, int[] dimensions, int population, int civ){
		int x = position.x;
		int y = position.y;
		if(civ != -1){
			boolean verify = spaceManager.verifySpace(position, dimensions, entities);
			civilizationAdditionMigrate(verify, civ, x, y, population);
		}
		receivedCiv++;
	}

	public void addOneCivilizationMigrate(Point position, int[] dimensions, int population, int civ,
										  String originalGroupID, String otherWorldInfo){
		int x = position.x;
		int y = position.y;
		if(civ != -1){
			boolean verify = spaceManager.verifySpace(position, dimensions, entities);
			civilizationAdditionMigrate(verify, civ, x, y, population, originalGroupID, otherWorldInfo);
		}
		receivedCiv++;
	}
	public void civilizationAdditionMigrate(boolean verificationOfSpace, int civ, int x, int y, int population){
		if (verificationOfSpace == true){
			switch(civ) {
			case 0:
				Civilization newCiv = new Mongol(x, y);
				entities.add(newCiv);
				newCiv.setPopulation(population);
				(new Thread(newCiv)).start();
				break;
			case 1:
				Civilization newCiv1 = new Roman(x, y);
				entities.add(newCiv1);
				newCiv1.setPopulation(population);
				(new Thread(newCiv1)).start();
				break;
			case 2:
				Civilization newCiv2 = new Greek(x, y);
				entities.add(newCiv2);
				newCiv2.setPopulation(population);
				(new Thread(newCiv2)).start();
				break;
			case 3:
				Civilization newCiv3 = new Persian(x, y);
				entities.add(newCiv3);
				newCiv3.setPopulation(population);
				(new Thread(newCiv3)).start();
				break;
			case 4:
				Civilization newCiv4 = new Egypcian(x, y);
				entities.add(newCiv4);
				newCiv4.setPopulation(population);
				(new Thread(newCiv4)).start();
				break;
			case 5:
				Civilization newCiv5 = new Viking(x, y);
				entities.add(newCiv5);
				newCiv5.setPopulation(population);
				(new Thread(newCiv5)).start();
				break;
			case 6:
				Civilization newCiv6 = new Chinese(x, y);
				entities.add(newCiv6);
				newCiv6.setPopulation(population);
				(new Thread(newCiv6)).start();
				break;
			case 7:
				Civilization newCiv7 = new Japanese(x, y);
				entities.add(newCiv7);
				newCiv7.setPopulation(population);
				(new Thread(newCiv7)).start();
				break;
			case 8:
				Civilization newCiv8 = new Goth(x, y);
				entities.add(newCiv8);
				newCiv8.setPopulation(population);
				(new Thread(newCiv8)).start();
				break;
			}

		}

	}
	
	public void civilizationAdditionMigrate(boolean verificationOfSpace, int civ, int x, int y, int population,
											String originalGroupID, String otherWorldInfo){
		if (verificationOfSpace == true){
			switch(civ) {
			case 0:
				Civilization newCiv = new Mongol(x, y);
				newCiv.setOriginalGroupID(originalGroupID);
				newCiv.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv);
				newCiv.setPopulation(population);
				(new Thread(newCiv)).start();
				break;
			case 1:
				Civilization newCiv1 = new Roman(x, y);
				newCiv1.setOriginalGroupID(originalGroupID);
				newCiv1.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv1);
				newCiv1.setPopulation(population);
				(new Thread(newCiv1)).start();
				break;
			case 2:
				Civilization newCiv2 = new Greek(x, y);
				newCiv2.setOriginalGroupID(originalGroupID);
				newCiv2.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv2);
				newCiv2.setPopulation(population);
				(new Thread(newCiv2)).start();
				break;
			case 3:
				Civilization newCiv3 = new Persian(x, y);
				newCiv3.setOriginalGroupID(originalGroupID);
				newCiv3.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv3);
				newCiv3.setPopulation(population);
				(new Thread(newCiv3)).start();
				break;
			case 4:
				Civilization newCiv4 = new Egypcian(x, y);
				newCiv4.setOriginalGroupID(originalGroupID);
				newCiv4.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv4);
				newCiv4.setPopulation(population);
				(new Thread(newCiv4)).start();
				break;
			case 5:
				Civilization newCiv5 = new Viking(x, y);
				newCiv5.setOriginalGroupID(originalGroupID);
				newCiv5.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv5);
				newCiv5.setPopulation(population);
				(new Thread(newCiv5)).start();
				break;
			case 6:
				Civilization newCiv6 = new Chinese(x, y);
				newCiv6.setOriginalGroupID(originalGroupID);
				newCiv6.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv6);
				newCiv6.setPopulation(population);
				(new Thread(newCiv6)).start();
				break;
			case 7:
				Civilization newCiv7 = new Japanese(x, y);
				newCiv7.setOriginalGroupID(originalGroupID);
				newCiv7.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv7);
				newCiv7.setPopulation(population);
				(new Thread(newCiv7)).start();
				break;
			case 8:
				Civilization newCiv8 = new Goth(x, y);
				newCiv8.setOriginalGroupID(originalGroupID);
				newCiv8.setOtherWorldInfo(otherWorldInfo);
				entities.add(newCiv8);
				newCiv8.setPopulation(population);
				(new Thread(newCiv8)).start();
				break;
			}

		}

	}
	
	public List<IpPort> getKnownAddresses() {
		return migrationManager.getKnownAddresses();
	}

	public void migrate(GameEntity entity, IpPort destination) {
		migrationManager.emigrate(entity, destination);
		migrated++;
		//MessageMediator messageMediator = MessageMediator.getInstance();
		//messageMediator.setSimulation("Se ha enviado una entidad de tipo " + entity.getClass().toString());
	}

	/**
	 * Entrega la entidad que se encuentra en la posici�n (x,y).
	 * @param x - coordenada x en la que se buscar�.
	 * @param y - coordenada y en la que se buscar�.
	 * @return la entidad que se encuentra en esa posici�n, null si est� vac�a.
	 */
	public GameEntity getEntityAt(int x, int y) {
		// TODO - adquirir un lock?

		// Buscamos en la lista de entidades.
		// TODO - ojal� fuera una lista de entidades y no solo civilizaciones.
		for (GameEntity entity : entities) {
			Point position = entity.getPosition();
			int[][] form = entity.getForm();

			// Bounding box entidad
			int startX = position.x;
			int endX = position.x + form.length;
			int startY = position.y;
			int endY = position.y + form[0].length;

			boolean xInside = (x >= startX) && (x < endX);
			boolean yInside = (y >= startY) && (y < endY);

			if (xInside && yInside) {
				return entity;
			}
		}

		// TODO - liberar el lock?
		return null;
	}
	Point point = new Point();
	public void finishSimulation(){
		
		
		for(GameEntity e : entities){
			e.setAlive(false);
			
		}
		int n = entities.size();
		Point point = new Point();
		point.x = 5*n+10;
		point.y = 5*n+10;
		//mapLocker = new MapLocker(n*5+10, n*5+10);
		mapLocker.take(new Point(), point); 
	}
	
}
