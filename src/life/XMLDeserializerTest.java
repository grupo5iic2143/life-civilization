package life;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import life.civilization.Civilization;
import life.civilization.Japanese;
import life.graphic.GameFrame;
import life.resource.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLDeserializerTest {

	XMLSerializer xmlSerializer;
	GameFrame frame;
	
	@Before
	/*
	 * Corresponde a un mundo de tamano grande, sin ninguna otra entidad
	 */
	public void setUp() throws Exception {
		xmlSerializer = new XMLSerializer();
		World w = World.getInstance();	
		w.setInitialCivilizationsAndResources(0, 0);
		w.setMap(new int[100][100]);
		w.setMapLocker(new MapLocker(100, 100));
		frame = new GameFrame();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDeserialize() throws ParserConfigurationException, SAXException, IOException {
		
		String xmlRecords = "<GameOfLife><Common><PosX>50</PosX><PosY>50</PosY><Width>3</Width><Height>4</Height>"
				+ "<OriginalGroupID>5</OriginalGroupID></Common><WorldSpecific><World id=\"4\"><Type>RIVER</Type>"
				+ "<Population>1000</Population></World></WorldSpecific></GameOfLife>";
		
		World.getInstance().getMigrationManager().getXMLSerializer().Deserialize(xmlRecords);
		if (!World.getInstance().getCivilizations().isEmpty()){
			for(GameEntity game_entity : World.getInstance().getCivilizations()){
				assertEquals("Incorrect type", "RIVER", ((Resource)game_entity).getType().name());
			}
		}
	}
	
	@Test
	public void testDeserializeOtherWorld(){
		
		String xmlRecords = "<GameOfLife><Common><PosX>50</PosX><PosY>50</PosY><Width>3</Width><Height>4</Height>"
				+ "<OriginalGroupID>4</OriginalGroupID></Common><WorldSpecific><World id=\"4\"><Type>RIVER</Type>"
				+ "<Population>1000</Population></World></WorldSpecific></GameOfLife>";
		
		World.getInstance().getMigrationManager().getXMLSerializer().Deserialize(xmlRecords);
		if (!World.getInstance().getCivilizations().isEmpty()){
			for(GameEntity game_entity : World.getInstance().getCivilizations()){
				assertFalse("Incorrect type", "RIVER"==((Civilization)game_entity).getType().name());
				assertFalse("Incorrect type", "RUINS"==((Civilization)game_entity).getType().name());
				assertFalse("Incorrect type", "FOOD"==((Civilization)game_entity).getType().name());
			}
		}
		
	}
		
		
	}


