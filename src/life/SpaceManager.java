package life;
import java.awt.Point;
import java.util.List;
public class SpaceManager {
	private int [][] map;

	public SpaceManager(){
		
	}
	
	
	//Posicion 0 de NewWidth_height guarda el ancho la posicion 1 la altura
	public boolean verifySpace(Point NewPosition,int[] NewWidth_height , List<GameEntity> Entities){
		
		//Dimensiones de las gamentities en la lista de elementos con los que se 
		//verifica la colision
	
		int entitywidth;
		int entityheight;
		int entityPosX;
		int entityPosY;
		
		//Verificar colisiones con todas las entities del mapa
		
		for(GameEntity game_entity : Entities)
		{
			
			entitywidth = game_entity.getWidth_height()[0];
			entityheight = game_entity.getWidth_height()[1];
			entityPosX = game_entity.getPosition().x;
			entityPosY = game_entity.getPosition().y;
			
			//Si la posicionX y la Y estan en el espacio que ocupa la entidad, bool falso
			if(((entityPosX < NewPosition.x &&  NewPosition.x < entityPosX + entitywidth) ||
				(entityPosX < NewPosition.x + NewWidth_height[0]  &&  NewPosition.x < entityPosX + entitywidth))
				&&
				((entityPosY < NewPosition.y &&  NewPosition.y < entityPosY + entitywidth) ||
				(entityPosY < NewPosition.y + NewWidth_height[1] &&  NewPosition.y < entityPosY + entityheight)))
			{
				//System.out.println("La entidad no se puede colocar en esta nueva posicion");
				return false; //No se puede colocar una entity en esa posicion
				
			}
			
			
		}
		
		
		//System.out.println("Se pudo colocar la entidad");
		return true;
	}
	
	public boolean verifySpaceEntity(GameEntity gameEntity,Point NewPosition,int[] NewWidth_height ,  List<? extends GameEntity> Entities)
	{
		//Dimensiones de las gamentities en la lista de elementos con los que se 
				//verifica la colision
				if(Entities==null)
				{
					//System.out.println("Entities es nulo en verifySpace");
					return true;
				}
				int entitywidth;
				int entityheight;
				int entityPosX;
				int entityPosY;
				
				//Verificar colisiones con todas las entities del mapa
				
				for(GameEntity game_entity : Entities)
				{
					if(game_entity != gameEntity) //Evita comparar posicion con la entidad que se quiere mover
					{
					entitywidth = game_entity.getWidth_height()[0];
					entityheight = game_entity.getWidth_height()[1];
					entityPosX = game_entity.getPosition().x;
					entityPosY = game_entity.getPosition().y;
					
					//Si la posicion X y la Y estan en el espacio que ocupa la entidad, bool falso
					if(((entityPosX < NewPosition.x &&  NewPosition.x < entityPosX + entitywidth) ||
						(entityPosX < NewPosition.x + NewWidth_height[0]  &&  NewPosition.x < entityPosX + entitywidth))
						&&
						((entityPosY < NewPosition.y &&  NewPosition.y < entityPosY + entitywidth) ||
						(entityPosY < NewPosition.y + NewWidth_height[1] &&  NewPosition.y < entityPosY + entityheight)))
					{
						return false; //No se puede colocar una entity en esa posicion
						
					}
					
					//En contacto con otras civilizaciones
					else if(((entityPosX <= NewPosition.x &&  NewPosition.x <= entityPosX + entitywidth) ||
							(entityPosX <= NewPosition.x + NewWidth_height[0]  &&  NewPosition.x <= entityPosX + entitywidth))
							&&
							((entityPosY <= NewPosition.y &&  NewPosition.y <= entityPosY + entitywidth) ||
							(entityPosY <= NewPosition.y + NewWidth_height[1] &&  NewPosition.y <= entityPosY + entityheight)))
					{
					      gameEntity.addCiv(game_entity);
					}
					}
				}
				return true;
		
	}
}
