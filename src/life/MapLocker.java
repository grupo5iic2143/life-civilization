package life;

import java.awt.Point;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MapLocker {
	
	Lock[][] locks;

	public MapLocker(int width, int height) {
		locks = new Lock[width][height];
		
		for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++) {
			locks[i][j] = new ReentrantLock();
		}
	}

	public synchronized void take(Point a, Point b) {
		int x1 = Math.max(0, a.x);
		int x2 = Math.min(locks.length, b.x);
		int y1 = Math.max(0, a.y);
		int y2 = Math.min(locks[0].length, b.y);
		
		for (int i = x1; i < x2; i++)
		for (int j = y1; j < y2; j++) {
			locks[i][j].lock();
		}
	}

	public void release(Point a, Point b) {
		int x1 = Math.max(0, a.x);
		int x2 = Math.min(locks.length, b.x);
		int y1 = Math.max(0, a.y);
		int y2 = Math.min(locks[0].length, b.y);
		
		for (int i = x1; i < x2; i++)
		for (int j = y1; j < y2; j++) {
			locks[i][j].unlock();
		}
	}

	public Point[] entityArea(GameEntity entity) {
		Point position = entity.getPosition();
		int[] widthHeight = entity.getWidth_height();

		Point a = new Point(position.x - 1, position.y - 1);
		Point b = new Point(position.x + widthHeight[0] + 1,
							position.y + widthHeight[1]);

		return new Point[]{a, b};
	}
	
	public Lock[][] getLocks(){
		return locks;
	}
}
