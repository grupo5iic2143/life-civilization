package life.graphic;

import life.World;

/**
 * Clase encargada de renderear el mapa periodicamente.
 */
public class MapRenderer implements Runnable {
	
	/**
	 * GameFrame que se rendeara.
	 */
	GameFrame gameFrame;

	/**
	 * Clase que renderea el mapa periodicamente.
	 * @param gameFrame donde se rendeara.
	 */
	public MapRenderer(GameFrame gameFrame) {
		this.gameFrame = gameFrame;
	}

	@Override
	public void run() {
		while (true) {
			gameFrame.DrawEntities(World.getInstance().getCivilizations());
		
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
