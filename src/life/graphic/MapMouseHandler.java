package life.graphic;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import life.graphic.event.MapClickCommand;

public class MapMouseHandler implements MouseListener {

	/**
	 * Mapa del juego.
	 */
	private GameMap gameMap;

	/**
	 * Se ejecuta cuando el usuario hace click en el mapa.
	 */
	private MapClickCommand mapClickCommand;
	
	public void setMapClickCommand(MapClickCommand mapClickCommand) {
		this.mapClickCommand = mapClickCommand;
	}
	
	public MapClickCommand getMapClickCommand() {
		return mapClickCommand;
	}

	/**
	 * Maneja los eventos de mouse en el mapa.
	 * @param gameMap - mapa que manejar�.
	 */
	public MapMouseHandler(GameMap gameMap) {
		this.gameMap = gameMap;
		gameMap.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent event) {
		int clickX = event.getX();
		int clickY = event.getY();

		int gridX = gameMap.containingCellX(clickX);
		int gridY = gameMap.containingCellY(clickY);
		
		mapClickCommand.click(gridX, gridY);
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		// TODO Auto-generated method stub

	}

}
