package life.graphic;

import java.awt.Color;
import java.awt.Point;
import java.util.List;

import life.GameEntity;

/**
 * Clase encargada de dibujar las entidades en el mapa.
 * @author Matu
 *
 */
public class EntityDrawer {
	
	/**
	 * Mapa en el que se dibujar�.
	 */
	private GameMap gameMap;
	
	/**
	 * Coordenada en el mundo del punto superior-izquierdo del rect�ngulo que se
	 * ver� (la incluye).
	 */
	private int x1, y1;

	/**
	 * Coordenada en el mundo del punto inferior-derecho del rect�ngulo que se
	 * ver� (no la incluye).
	 */
	private int x2, y2;

	/**
	 * Entrega la coordenada x de la esquina superior-izquierda.
	 * @return la coordenada.
	 */
	public int getX1() {
		return x1;
	}

	/**
	 * Entrega la coordenada y de la esquina superior-izquierda.
	 * @return la coordenada.
	 */
	public int getY1() {
		return y1;
	}

	/**
	 * Entrega la coordenada x de la esquina inferior-derecha.
	 * @return la coordenada.
	 */
	public int getX2() {
		return x2;
	}

	/**
	 * Entrega la coordenada y de la esquina inferior-derecha.
	 * @return la coordenada.
	 */
	public int getY2() {
		return y2;
	}

	public EntityDrawer(GameMap gameMap) {
		this.gameMap = gameMap;
		
		// coordenadas por defecto
		x1 = 0;
		y1 = 0;
		x2 = 50;
		y2 = 50;
	}
	
	/**
	 * Entrega el ancho del visor.
	 * @return tama�o del ancho del visor.
	 */
	public int getViewportWidth() {
		return x2 - x1;
	}
	
	/**
	 * Entrega el alto del visor.
	 * @return tama�o del alto del visor.
	 */
	public int getViewportHeight() {
		return y2 - y1;
	}

	/**
	 * Fija las coordenadas del espacio que se mostrar�.
	 * @param x1 - coordenada x punto superior-izquierdo (inclu�da).
	 * @param y1 - coordenada y punto superior-izquierdo (inclu�da).
	 * @param x2 - coordenada x punto inferior-derecho (no inclu�da).
	 * @param y2 - coordenada y punto inferior-derecho (no inclu�da).
	 */
	public void setViewport(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	/**
	 * Mueve el visor hacia arriba.
	 * @param n - cantidad de espacios que se mueve.
	 */
	public void moveViewportUp(int n) {
		y1 -= n;
		y2 -= n;
	}

	/**
	 * Mueve el visor hacia la derecha.
	 * @param n - cantidad de espacios que se mueve.
	 */
	public void moveViewportRight(int n) {
		x1 += n;
		x2 += n;
	}

	/**
	 * Mueve el visor hacia abajo.
	 * @param n - cantidad de espacios que se mueve.
	 */
	public void moveViewportDown(int n) {
		y1 += n;
		y2 += n;
	}

	/**
	 * Mueve el visor hacia la izquierda.
	 * @param n - cantidad de espacios que se mueve.
	 */
	public void moveViewportLeft(int n) {
		x1 -= n;
		x2 -= n;
	}
	
	/**
	 * Agranda el visor.
	 * @param n cantidad de unidades que se agranda.
	 */
	public void enlargeViewport(int n) {
		x1 -= n;
		y1 -= n;
		x2 += n;
		y2 += n;
	}

	/**
	 * Achica el visor.
	 * @param n cantidad de unidades que se achicara.
	 */
	public void shrinkViewport(int n) {
		x1 += n;
		y1 += n;
		x2 -= n;
		y2 -= n;
	}
	
	/**
	 * Dibuja la lista de entidades en el mapa.
	 * @param entities - entidades que se dibujar�n.
	 */
	public void DrawEntities(List<? extends GameEntity> entities) {
		// TODO - adquirir lock de la lista?

		// Creamos el mapa donde dibujaremos
		Color[][] grid = new Color[getViewportWidth()][getViewportHeight()];
		
		for (GameEntity entity : entities) {
			int[][] shape = entity.getForm();
			Point position = entity.getPosition();
			int width = shape.length;
			int height = shape[0].length;
			
			// Si est� fuera del visor continuamos
			if (!isInsideViewport(entity)) continue;

			// Determinamos nuestros l�mites de iteraci�n
			int xMin = Math.max(x1, position.x);
			int xMax = Math.min(x2, position.x + width);
			int yMin = Math.max(y1, position.y);
			int yMax = Math.min(y2, position.y + height);
			
			for (int i = xMin; i < xMax; i++) {
				// Calculo de las coordenadas relativas
				int gridI = i - x1;
				int shapeI = i - position.x;
				
				for (int j = yMin; j < yMax; j++) {
					int gridJ = j - y1;
					int shapeJ = j - position.y;
					
					if (shape[shapeI][shapeJ] != 0) {
						grid[gridI][gridJ] = entity.getEntityColor();
					}
				}
			}
		}
		
		gameMap.setGrid(grid);
		
		// TODO - soltar el lock?
	}
	
	/**
	 * Determina si la entidad con las coordenadas dadas est� dentro del "visor".
	 * @param x - coordenada x superior-izquierda.
	 * @param y - coordenada y superior-izquierda.
	 * @param width - ancho de la entidad.
	 * @param height - alto de la entidad.
	 * @return boolean que indica si est� dentro del visor.
	 */
	private boolean isInsideViewport(int x, int y, int width, int height) {
		boolean xOverlap = ( (x + width) >= x1 ) && (x < x2);
		boolean yOverlap = ( (y + height) >= y1 ) && (y < y2);

		return xOverlap && yOverlap;
	}
	
	/**
	 * Determina si la entidad dada est� dentro del "visor".
	 * @param entity - entidad que se probar�.
	 * @return boolean que indica si est� dentro del visor.
	 */
	private boolean isInsideViewport(GameEntity entity) {
        int[][] shape = entity.getForm();
        Point position = entity.getPosition();
        return isInsideViewport
        	(position.x, position.y, shape.length, shape[0].length);
	}

}
