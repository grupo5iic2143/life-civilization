package life.graphic;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;

import life.World;

public class InitialConditionsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textHumidity;
	private JTextField textTemperature;
	private JTextField textCivilizations;
	private JTextField textResources;
	private JTextField textTime;
	private ButtonGroup group;
	private JTextField friendAddress;
	private JRadioButton climaTempladoButton;
	private JRadioButton climaDeserticoButton;
	private JRadioButton climaPolarButton;
	private JRadioButton climaMontanosoButton;
	
	
	private String friendIP;
	private String friendPort;
	
	/*
	 * Input limits.
	 */
	private final int minHumidity = 0;
	private final int maxHumidity = 100;
	private final int minTemperature = -20;
	private final int maxTemperature = 50;
	private final int minCivilizations = 0;
	private final int maxCivilizations = 300;
	private final int minResources = 0;
	private final int maxResources = 10;
	private final int minTime = 1;
	private final int maxTime = 900;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			InitialConditionsDialog dialog = new InitialConditionsDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public InitialConditionsDialog() {
		setTitle("Introduzca condiciones iniciales");
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 393, 216);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] {30, 50, 100, 30};
		gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		contentPanel.setLayout(gbl_contentPanel);
		this.setSize(400, 300);
		this.setMinimumSize(new Dimension(400,300));
		
		{
			JLabel lblClima = new JLabel("Seleccione clima que desea");
			GridBagConstraints gbc_lblClima = new GridBagConstraints();
			gbc_lblClima.anchor = GridBagConstraints.WEST;
			gbc_lblClima.insets = new Insets(0, 0, 5, 5);
			gbc_lblClima.gridx = 1;
			gbc_lblClima.gridy = 0;
			contentPanel.add(lblClima, gbc_lblClima);
		}
		
		GridBagConstraints gbc_Clima = new GridBagConstraints();
		gbc_Clima.anchor = GridBagConstraints.WEST;
		gbc_Clima.insets = new Insets(0, 0, 2, 2);
		gbc_Clima.gridx = 1;
		gbc_Clima.gridy = 1;
		
		{
		climaTempladoButton = new JRadioButton("Templado");
		climaTempladoButton.setSelected(true);
		contentPanel.add(climaTempladoButton, gbc_Clima);
		
		
	    climaDeserticoButton = new JRadioButton("Desertico");
	    climaDeserticoButton.setSelected(true);
	    gbc_Clima.gridy = 2;
	    contentPanel.add(climaDeserticoButton, gbc_Clima);
	    
	    climaPolarButton = new JRadioButton("Polar");
	    climaPolarButton.setSelected(true);
	    gbc_Clima.gridy = 3;
	    contentPanel.add(climaPolarButton, gbc_Clima);

	    climaMontanosoButton = new JRadioButton("Montanoso");
	    climaMontanosoButton.setSelected(true);
	    gbc_Clima.gridy = 4;
	    contentPanel.add(climaMontanosoButton, gbc_Clima);
	    group = new ButtonGroup();
	    group.add(climaDeserticoButton);
	    group.add(climaPolarButton);
	    group.add(climaTempladoButton);
	    group.add(climaMontanosoButton);
		}  
		{
			JLabel lblCivilizations = new JLabel("Initial civilizations");
			GridBagConstraints gbc_lblCivilizations = new GridBagConstraints();
			gbc_lblCivilizations.anchor = GridBagConstraints.WEST;
			gbc_lblCivilizations.insets = new Insets(0, 0, 5, 5);
			gbc_lblCivilizations.gridx = 1;
			gbc_lblCivilizations.gridy = 5;
			contentPanel.add(lblCivilizations, gbc_lblCivilizations);
		}
		{
			textCivilizations = new JTextField();
			GridBagConstraints gbc_textCivilizations = new GridBagConstraints();
			gbc_textCivilizations.insets = new Insets(0, 0, 5, 0);
			gbc_textCivilizations.fill = GridBagConstraints.HORIZONTAL;
			gbc_textCivilizations.gridx = 2;
			gbc_textCivilizations.gridy = 5;
			contentPanel.add(textCivilizations, gbc_textCivilizations);
			textCivilizations.setColumns(10);
		}
		{
			JLabel lblResources = new JLabel("Initial resources");
			GridBagConstraints gbc_lblResources = new GridBagConstraints();
			gbc_lblResources.anchor = GridBagConstraints.WEST;
			gbc_lblResources.insets = new Insets(0, 0, 5, 5);
			gbc_lblResources.gridx = 1;
			gbc_lblResources.gridy = 6;
			contentPanel.add(lblResources, gbc_lblResources);
		}
		{
			textResources = new JTextField();
			GridBagConstraints gbc_textResources = new GridBagConstraints();
			gbc_textResources.insets = new Insets(0, 0, 5, 0);
			gbc_textResources.fill = GridBagConstraints.HORIZONTAL;
			gbc_textResources.gridx = 2;
			gbc_textResources.gridy = 6;
			contentPanel.add(textResources, gbc_textResources);
			textResources.setColumns(10);
		}
		{
			JLabel lblTime = new JLabel("Simulation Time (seconds)");
			GridBagConstraints gbc_lblTime = new GridBagConstraints();
			gbc_lblTime.anchor = GridBagConstraints.WEST;
			gbc_lblTime.insets = new Insets(0, 0, 5, 5);
			gbc_lblTime.gridx = 1;
			gbc_lblTime.gridy = 7;
			contentPanel.add(lblTime, gbc_lblTime);
		}
		{
			textTime = new JTextField();
			GridBagConstraints gbc_textTime = new GridBagConstraints();
			gbc_textTime.insets = new Insets(0, 0, 5, 0);
			gbc_textTime.fill = GridBagConstraints.HORIZONTAL;
			gbc_textTime.gridx = 2;
			gbc_textTime.gridy = 7;
			contentPanel.add(textTime, gbc_textTime);
			textTime.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						validateInput();
						setClima();
						World.getInstance().setTime(getTime());
					}
				});
				
				okButton.setActionCommand("OK");				
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
	
	/**
	 * Valida el input del usuario.
	 */
	private void validateInput() {
		//Integer humidity = getHumidity();
		//Integer temperature = getTemperature();
		Integer civilizations = getCivilizations();
		Integer resources = getResources();
		Integer time = getTime();

		
		if (civilizations == null || !numberBetween(civilizations, 0, 300)) {
			showBetweenError("un numero de civilizaciones", minCivilizations, maxCivilizations);
			return;
		}
		if (resources == null || !numberBetween(resources, 0, 10)){
			showBetweenError("un numero de recursos", minResources, maxResources);
			return;
		}
		if (time == null || !numberBetween(time, 1, 900)){
			showBetweenError("un numero de segundos", minTime, maxTime);
			return;
		}

		setVisible(false);
	}
	
	/**
	 * Transforma un String en numero.
	 * @param string lo que se transformara.
	 * @return Integer con el valor deseado o null.
	 */
	private Integer toNumber(String string) {
		Integer result;

		try {
			result = Integer.valueOf(string);
        }
		catch (NumberFormatException e) {
			result = null;
		}
		
		return result;
	}

	/**
	 * Verifica si el numero esta en un rango.
	 * @param num numero que se verifica.
	 * @param min minimo valor (inclusivo).
	 * @param max maximo valor (inclusivo).
	 * @return true si esta en el rango.
	 */
	private boolean numberBetween(int num, int min, int max) {
		return min <= num && num <= max;
	}
	
	/**
	 * Muestra un mensaje de error.
	 * @param name nombre del atributo.
	 * @param min minimo valor que puede tomar.
	 * @param max maximo valor que puede tomar.
	 */
	private void showBetweenError(String name, int min, int max) {
		showError("Por favor introduzca " + name + " entre " +
                   min + " y " + max + ".");
	}
	
	/**
	 * Muestra un error al usuario.
	 * @param error texto que se mostrara.
	 */
	private void showError(String error) {
		JOptionPane.showMessageDialog(this,
                    				  error,
                                      "Intente nuevamente",
                                      JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Entrega el tiempo ingresado.
	 * @return tiempo ingresada en segundos.
	 */	
	public Integer getTime() {
		return toNumber(textTime.getText());
	}
	

	/**
	 * Entrega el numero de civilizaciones ingresado.
	 * @return numero de civilizaciones ingresado.
	 */
	public Integer getCivilizations() {
		return toNumber(textCivilizations.getText());
	}
	/**
	 * Entrega el numero de recursos ingresado
	 * @return numero de recursos ingresados
	 */
	public Integer getResources(){
		return toNumber(textResources.getText());
	}
	
	public void setClima(){
		
		if(climaDeserticoButton.isSelected() == true){
			World.getInstance().setWeather(0);			
		}
		else if(climaMontanosoButton.isSelected() == true){
			World.getInstance().setWeather(3);
		}		
		else if(climaPolarButton.isSelected() == true){
			World.getInstance().setWeather(2);
		}
		else if(climaTempladoButton.isSelected() == true){
			World.getInstance().setWeather(1);
		}
	}
}
