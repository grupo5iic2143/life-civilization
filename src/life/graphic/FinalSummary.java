package life.graphic;

import java.awt.*;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList; 
import java.util.List; 

import life.*;
import life.civilization.Egypcian;
import life.civilization.Type;;

public class FinalSummary extends JFrame  implements ActionListener{
	
	private final JTable contentPanel = new JTable();
	private String finalCivs;
	private String deadCivs;
	private String newCivs;
	private int egypcians;
	private int goths;
	private int greeks;
	private int japanese;
	private int mongols;
	private int persians;
	private int romans;
	private int viking;
	private int chinese;
	private int food;
	private int rivers;
	private int ruins;
	
	private int appearences;
	private int civCreated;
	private int civReceived;
	private int civMigrated;
	private int fusions;
	private int civDead;
	private int resCreated;
	private int resConsumed;
	private int conquest;
	private int resReceived;
	private JButton buttonExit;
	private JButton buttonNewSim;
	private GameFrame gameFrame;
	private JSplitPane jsp;
	
	private JPanel jpanel;
	private JPanel jpanelLeft;
	private JTextArea text;
	private List<GameEntity> entities;
	
	
	
	public FinalSummary(){
		
		jpanel= new JPanel();
		jpanelLeft= new JPanel(new GridLayout(2,1));
		jpanel.setLayout(new GridLayout(2,1));
		JScrollPane outer = new JScrollPane(jpanel);
		jsp = new JSplitPane();
		
		
		egypcians = 0;
		goths = 0;
		greeks = 0;
		japanese = 0;
		mongols = 0;
		persians = 0;
		romans = 0;
		viking = 0;
		chinese = 0;
		food = 0;
		rivers = 0;;
		ruins = 0;
		entities = World.getInstance().getCivilizations();
		
		for(GameEntity ge : entities){
			life.civilization.Type type = ge.getType();
			switch(type) {
			case MONGOL: mongols++; break;
			case ROMAN: romans++; break;
			case CHINESE: chinese++; break;
			case JAPANESE: japanese++; break;
			case GOTH: goths++; break;
			case GREEK: greeks++; break;
			case EGYPCIAN: egypcians++; break;
			case PERSIAN: persians++; break;
			case VIKING: viking++; break;
			case FOOD: food++; break;
			case RIVER: rivers++; break;
			case RUINS: ruins++; break;
											
			}			
		}
		
		
		Object[][] data = { 
				{"Mongol", mongols}, 
				{"Roman",romans},
				{"Chinese", chinese},
				{"Japanese", japanese},
				{"Goth", goths},
				{"Greek", greeks},
				{"Egypcian", egypcians},
				{"Persian", persians},
				{"Viking", viking},
				{"Food", food},
				{"River", rivers},
				{"Ruin", ruins}		
				}; 
		
		String[] columnNames = {"Tipo de Entidad", 
				"Cantidad", 
				};
		
		DefaultTableModel dtm = new DefaultTableModel(data, columnNames); 
		JTable table = new JTable(dtm);
		table.setEnabled(false);
		table.setPreferredScrollableViewportSize(new Dimension(500, 80));
		
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
		
		JScrollPane inner = new JScrollPane(table);
        outer.addMouseWheelListener(inner.getMouseWheelListeners()[0]);
        inner.removeMouseWheelListener(inner.getMouseWheelListeners()[0]);
        jpanel.add( inner);
								
		
		
		//Tabla 2
		String[] columnNames2 = {"Dato", 
				"Cantidad", 
				};
		{			
		appearences = World.getInstance().getAppearances();
		civCreated = World.getInstance().getCivCreated();
		civReceived = World.getInstance().getReceiverCiv();
		civMigrated = World.getInstance().getMigrated();
		fusions = World.getInstance().getFusions();
		civDead = World.getInstance().getCivDeads();
		resCreated = World.getInstance().getResCreated();
		resConsumed = World.getInstance().getResEaten();
		conquest = World.getInstance().getConquest();
		resReceived = World.getInstance().getReceiverRes();
		}
		
		Object[][] data2 = { 
				{"Apariciones por clima", appearences}, 
				{"Civilizaciones creadas por el usuario",civCreated},
				{"Recursos creados por el usuario", resCreated},
				{"Fusiones entre civilizaciones por negociación", fusions},
				{"Fusiones entre civilizaciones por conquista", conquest},
				{"Civilizaciones muertas en batalla",  civDead},
				{"Entidades enviadas a otro mundo", civMigrated},
				{"Civilizaciones recibidad desde otro mundo", civReceived},
				{"Recursos recibidad desde otro mundo", resReceived},
				{"Recursos consumidos", resConsumed}		
				}; 
		
		DefaultTableModel dtm2 = new DefaultTableModel(data2, columnNames2);
		JTable table2 = new JTable(dtm2);
		table2.setEnabled(false);
		table2.setPreferredScrollableViewportSize(new Dimension(500, 80));
		table2.getColumnModel().getColumn(1).setCellRenderer(tcr);
		JScrollPane inner2 = new JScrollPane(table2);
        outer.addMouseWheelListener(inner2.getMouseWheelListeners()[0]);
        inner.removeMouseWheelListener(inner2.getMouseWheelListeners()[0]);
        jpanel.add(inner2);
        
        
        buttonExit = new JButton("Salir del Programa");  
        buttonExit.addActionListener(this);
        buttonExit.setBounds(300,250,100,30);

        buttonNewSim = new JButton("Comenzar nueva simulacion");
        buttonNewSim.addActionListener(this);
        buttonNewSim.setBounds(300,250,100,30);


        jpanelLeft.add(buttonNewSim);
        jpanelLeft.add(buttonExit);
        
        jsp.setLeftComponent(outer);
        jsp.setRightComponent(jpanelLeft);
        jsp.setResizeWeight(0.90);
        jsp.setDividerLocation(0.85);
        
		setTitle("Fin de la Simulacion - Summary");	
		this.add(jsp);
		this.setVisible(true);
		
		this.setMinimumSize(new Dimension(900,480));
		/*addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) { 
			System.exit(0); 
			} 
			}); */
			
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()==buttonExit) {
            System.exit(0);
        }
		if (e.getSource()==buttonNewSim) {
			
			this.dispose ();
			gameFrame.Close();
			GameFrame.main(null);
        }
		
	}
	
	public void gameFrame(GameFrame gf){	
		gameFrame = gf;
		
	}
	
	
	

}
