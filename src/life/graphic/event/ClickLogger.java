package life.graphic.event;

import java.awt.Point;

import life.graphic.EntityDrawer;
import life.graphic.GameFrame;

public class ClickLogger implements MapClickCommand {

	private GameFrame gameFrame;
	private Point point;
	private EntityDrawer entityDrawer;

	public ClickLogger(GameFrame gameFrame, EntityDrawer entityDrawer) {
		this.gameFrame = gameFrame;
		this.entityDrawer = entityDrawer;
	}

	@Override
	public void click(int x, int y) {
		int positionX = entityDrawer.getX1() + x;
		int positionY = entityDrawer.getY1() + y;
		
		gameFrame.PrintMessageClick("Click en (" + x + ", " + y + ")");
		point = new Point();
		point.setLocation(positionX, positionY);
		gameFrame.addNewEntity(point);
	}
	/**
	 * Retorna el punto en que hizo click
	 * @return point
	 */
	public Point returnUbication(){
		return point;
	}
	

}
