package life.graphic.event;

/**
 * Representa un comando que se lleva a cabo cuando el usuario hace click en una
 * celda del mapa.
 * @author Matu
 */
public interface MapClickCommand {

	/**
	 * Se ejecuta cuando se hace click en una celda del mapa.
	 * @param x - coordenada x de la celda.
	 * @param y - coordenada y de la celda.
	 */
	public void click(int x, int y);
}
