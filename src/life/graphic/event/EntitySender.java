package life.graphic.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import life.GameEntity;
import life.World;
import life.graphic.EntityDrawer;
import life.graphic.GameFrame;
import life.network.MigrationManager.IpPort;

public class EntitySender implements MapClickCommand, ActionListener {
	
	private GameFrame gameFrame;
	private EntityDrawer entityDrawer;
	private MapClickCommand original;

	public EntitySender(GameFrame gameFrame, EntityDrawer entityDrawer) {
		this.gameFrame = gameFrame;
		this.entityDrawer = entityDrawer;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		original = gameFrame.getMapClickCommand();
		gameFrame.setMapClickCommand(this);
	}

	@Override
	public void click(int x, int y) {
		int positionX = entityDrawer.getX1() + x;
		int positionY = entityDrawer.getY1() + y;

		GameEntity entity = World.getInstance().getEntityAt(positionX, positionY);
		IpPort address = gameFrame.getSelectedAddress();
		
		World.getInstance().migrate(entity, address);

		gameFrame.setMapClickCommand(original);
		gameFrame.getGameMap().setFocusable(true);
		gameFrame.getGameMap().requestFocusInWindow();
	}
}
