package life.graphic.event;

import life.GameEntity;
import life.World;
import life.graphic.EntityDrawer;
import life.graphic.GameFrame;
import life.resource.Resource;
import life.civilization.Civilization;
public class EntityClicker implements MapClickCommand {

	private GameFrame gameFrame;
	private EntityDrawer entityDrawer;

	public EntityClicker(GameFrame gameFrame, EntityDrawer entityDrawer) {
		this.gameFrame = gameFrame;
		this.entityDrawer = entityDrawer;
	}

	@Override
	public void click(int x, int y) {
		int positionX = entityDrawer.getX1() + x;
		int positionY = entityDrawer.getY1() + y;

		GameEntity entity = World.getInstance().getEntityAt(positionX, positionY);

		//World.getInstance().finishSimulation();
		if(entity != null) {
			if(entity instanceof Civilization){
				Civilization c = (Civilization) entity;
				gameFrame.PrintMessageClick("Tipo: " + c.getType() + "\nPoblacion: " + c.getPopulation() + "\nNivel: " + c.getLevel()
											+ "\nVelocidad: "+ c.getSwiftness() + "\nMovimiento: " + c.getMoverange());

			}
			if(entity instanceof Resource){
				Resource r = (Resource) entity;
				gameFrame.PrintMessageClick("Tipo: " + r.getType() + "\nCantidad: " + r.getQuantity() + "\nRegeneracion: " + r.getRegeneration());
			}
		}
	}

}
