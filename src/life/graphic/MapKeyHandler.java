package life.graphic;

import java.awt.RenderingHints.Key;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MapKeyHandler implements KeyListener {
	
	private GameMap gameMap;
	private EntityDrawer entityDrawer;

	public MapKeyHandler(GameMap gameMap, EntityDrawer entityDrawer) {
		this.gameMap = gameMap;
		this.entityDrawer = entityDrawer;
		gameMap.addKeyListener(this);
	}

	@Override
	public void keyPressed(KeyEvent event) {
		int keyCode = event.getKeyCode();
		
		switch (keyCode) {
		case KeyEvent.VK_UP:
		case KeyEvent.VK_W:
			entityDrawer.moveViewportUp(1);
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_D:
			entityDrawer.moveViewportRight(1);
			break;
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_S:
			entityDrawer.moveViewportDown(1);
			break;
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_A:
			entityDrawer.moveViewportLeft(1);
			break;
		case KeyEvent.VK_PLUS:
		case KeyEvent.VK_E:
			entityDrawer.shrinkViewport(1);
			break;
		case KeyEvent.VK_MINUS:
		case KeyEvent.VK_Q:
			entityDrawer.enlargeViewport(1);
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent event) {
		// TODO Auto-generated method stub

	}

}
