package life.graphic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

/**
 * Representa el mapa del juego.
 */
public class GameMap extends JPanel {
	
	/**
	 * Representa la grilla.
	 * 	0: vacio
	 * 	1: ocupado
	 */
	private Color[][] grid;
	
	/**
	 * Lock que regula el acceso y edicion de la grilla.
	 */
	private Object gridLock = new Object();

	/**
	 * Tama�o de una celda.
	 */
	private double cellSize;
	
	/**
	 * El ancho de la grilla que se muestra.
	 * @return el tamano como int.
	 */
	private int getGridWidth() {
		return grid.length;
	}
	
	/**
	 * El alto de la grilla que se muestra.
	 * @return el tamano como int.
	 */
	private int getGridHeight() {
		return grid[0].length;
	}

	/**
	 * Una grilla para representar el mapa del juego.
	 * @param gridWidth numero de bloques de ancho.
	 * @param gridHeight numero de bloques de alto.
	 */
	public GameMap(int gridWidth, int gridHeight) {
		grid = new Color[gridWidth][gridHeight];
	}
	
	/**
	 * Construye una grilla con alto y ancho por defecto (50).
	 */
	public GameMap() {
		this(50, 50);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,
                RenderingHints.VALUE_STROKE_PURE);
		
		synchronized(gridLock) {
			double width = getWidth();
			double height = getHeight();
		
			int gridWidth = getGridWidth();
			int gridHeight = getGridHeight();
		
			// Buscamos el tamano que deben tener las celdas para que quepan
			cellSize = Math.min(width/gridWidth, height/gridHeight);
			
			// Color de fondo
			Shape l = new Rectangle2D.Double
					(0, 0, gridWidth*cellSize, gridHeight*cellSize);
			g.setColor(new Color(0x222222));
			g2.fill(l);
			
			// Llenar celdas
			// TODO - ver como se deciden los colores
			g.setColor(new Color(0xFF0000));
		
			for(int i = 0; i < gridWidth; i++) {
				for(int j = 0; j < gridHeight; j++) {
					if (grid[i][j] != null) {
						g.setColor(grid[i][j]);
						drawCell(i, j, g2);
					}
				}
			}

			g.setColor(new Color(0x151515));
			// Dibujamos la grilla
			drawGrid(gridWidth, gridHeight, g2);
		}
	}
	
	/**
	 * Dibuja una celda dentro de la grilla. Es usado por paintComponent.
	 * @param x coordenada de grilla x
	 * @param y coordenada de grilla y
	 * @param g grafico que se va a pintar
	 */
	private void drawCell(double x, double y, Graphics2D g) {
		Shape l = new Rectangle2D.Double
				(x*cellSize, y*cellSize, cellSize, cellSize);
		g.fill(l);
	}
	
	/**
	 * Dibuja la grilla en la pantalla.
	 * @param gridWidth ancho de la grilla.
	 * @param gridHeight alto de la grilla.
	 * @param g grafico en el que se dibujara.
	 */
	private void drawGrid(int gridWidth, int gridHeight, Graphics2D g) {
        // Ancho y Largo "reales"
        double width = cellSize*gridWidth;
        double height = cellSize*gridHeight;

        // Lineas horizontales
        for(int i = 0; i <= gridWidth; i++) {
        	Shape l = new Line2D.Double(i*cellSize, 0, i*cellSize, height);
        	g.draw(l);
        }

        // Lineas verticales
        for(int j = 0; j <= gridHeight; j++) {
            Shape l = new Line2D.Double(0, j*cellSize, width, j*cellSize);
            g.draw(l);
        }	
	}
	
	/**
	 * Setea la grilla con lo que se desea mostrar.
	 * @param grid matriz con la informacion que se mostrara.
	 */
	public void setGrid(Color[][] grid) {
		synchronized(gridLock) {
			this.grid = grid;
		}
		repaint();
	}
	
	/**
	 * Entrega la coordenada x de la celda que contiene al pixel x.
	 * @param x coordenada del pixel.
	 */
	public int containingCellX(int x) {
		return (int) Math.floor(x/cellSize);
	}

	/**
	 * Entrega la coordenada y de la celda que contiene al pixel y.
	 * @param y coordenada del pixel.
	 */
	public int containingCellY(int y) {
		return (int) Math.floor(y/cellSize);
	}
}
