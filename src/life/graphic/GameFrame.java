package life.graphic;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.awt.TextArea;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.print.attribute.standard.PrinterMessageFromOperator;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;
import javax.swing.text.MaskFormatter;
import javax.swing.*;

import life.*;
import life.civilization.*;
import life.network.MigrationManager.IpPort;
import life.resource.*;
import life.GameEntity;
import life.civilization.Chinese;
import life.graphic.event.ClickLogger;
import life.graphic.event.EntityClicker;
import life.graphic.event.EntitySender;
import life.graphic.event.MapClickCommand;

public class GameFrame extends JFrame implements ActionListener {

	private JPanel contentPane;
	private GameMap gameMap;
	private EntityDrawer entityDrawer;
	private JTextArea messagePane;
	private JTextArea messagePaneClick;
	private JTextArea messagePaneSimulation;
	private JComboBox jComboBox1;
	private JComboBox addressComboBox;
	private JButton jbuttonCreator;
	private JButton jbuttonSender;
	private MapMouseHandler mapMouseHandler2;
	private MessageMediator messageMediator;
	private static Timer timer;
	private JLabel lbl_time;
	private int seconds;
	private int maxTime;
	private static CreatorCiv creator;
	private static GameFrame gf= null;
	private FinalSummary fs;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Pedimos condiciones iniciales
					
					InitialConditionsDialog init = new InitialConditionsDialog();
					init.setVisible(true);

					// Creamos el mundo
                    World w = World.getInstance();		
                    w.setInitialCivilizationsAndResources(init.getCivilizations(), init.getResources());                                

                    // Creamos la ventana principal
					GameFrame frame = new GameFrame();
					frame.setVisible(true);
					frame.initiateRendering();
					
					creator = new CreatorCiv();
					

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public GameFrame() {
		
		
		// Armar ventana
		setTitle("LIFE-CIVILIZATION");	
		this.setSize(600, 450);
		this.setMinimumSize(new Dimension(600,450));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		contentPane = new JPanel();		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridBagLayout());
		

		JSplitPane splitPane = new JSplitPane();		
		splitPane.setResizeWeight(0.85);
		
		
		// Mapa de juego
		GameMap gameMap = new GameMap();
		this.gameMap = gameMap;
		splitPane.setLeftComponent(gameMap);			
		
		gameMap.setFocusable(true);
		gameMap.requestFocusInWindow();
						
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		this.messagePane = textArea;
		textArea.setEditable(false);
		//scrollPane.setViewportView(textArea);
		
		//scrollpane para informacion de clicks
		JScrollPane scrollPaneClick = new JScrollPane();
		scrollPaneClick.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPaneClick.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		
		JTextArea textAreaClick = new JTextArea();
		textAreaClick.setLineWrap(true);
		this.messagePaneClick = textAreaClick;
		textAreaClick.setEditable(false);
		scrollPaneClick.setViewportView(textAreaClick);
		
		//scrollpane para informaci�n de simulacion
		JScrollPane scrollPaneSimulation = new JScrollPane();
		scrollPaneSimulation.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPaneSimulation.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		
		JTextArea textAreaSimulation = new JTextArea();
		textAreaSimulation.setLineWrap(true);
		this.messagePaneSimulation = textAreaSimulation;
		textAreaSimulation.setEditable(false);
		DefaultCaret caret = (DefaultCaret)textAreaSimulation.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		scrollPaneSimulation.setViewportView(textAreaSimulation);


		// crear combo box para insertar civ
		String[] civs ={"Chinese","Egipcian","Goth","Greek","Japanese","Mongol", "Persian", "Roman", "Viking", "Food", "River"};
		jComboBox1 = new JComboBox<>(civs);
		
		// crear combo box para mandar entidades
		List<IpPort> addressList = World.getInstance().getKnownAddresses();
		IpPort[] addressArray = addressList.toArray(new IpPort[addressList.size()]);
		addressComboBox = new JComboBox<IpPort>(addressArray);
		
		//creamos un panel donde se pondran las distintas zonas de texto
		JPanel jpane = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		
			
		JLabel labelTitulo = new JLabel("Seleccione entidad que desea agregar:");		
		
		c.gridy = 0; 
		c.gridheight = 1;
		c.gridwidth = 0;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 0.0;
		jpane.add(labelTitulo, c);
		
		c.gridy = 1;
		c.weightx = 1.0;
		c.weighty = 0.0;
		jpane.add(jComboBox1,c);
		
		//--------------crear boton para crear entidades
		
		jbuttonCreator = new JButton("Confirmar entidad");
		jbuttonCreator.addActionListener(this);
		c.gridy = 2; 
		c.gridx = 0;
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 0;
		c.weightx = 0.0;
		jpane.add(jbuttonCreator, c);
		
		
		// Envio de entidades
		JLabel labelAddress = new JLabel("Seleccione donde desea enviar la entidad:");		

		c.gridy = 3; 
		c.gridheight = 1;
		c.gridwidth = 0;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 0.0;
		jpane.add(labelAddress, c);
		
		c.gridy = 4;
		c.weightx = 1.0;
		c.weighty = 0.0;
		jpane.add(addressComboBox,c);
		
		//boton para crear entidades y boton de envio
		
		jbuttonSender = new JButton("Realizar envio");
		c.gridy = 5; 
		c.gridx = 0;
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 0;
		c.weightx = 0.0;
		jpane.add(jbuttonSender, c);
		
			
		JLabel labelClick = new JLabel("Informacion de click:");		
		c.gridy = 6;
		c.gridx = 0;
		c.gridwidth =0;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.0;
		jpane.add(labelClick, c);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridy = 7;
		c.gridx = 0;
		c.gridwidth =0;
		c.weighty = 1.0;
		jpane.add(scrollPaneClick,c);
		
		JLabel labelSimulation = new JLabel("Informacion de simulacion:");
		c.gridy = 8; 
		c.gridx = 0;
		c.gridwidth =0;
		c.anchor = GridBagConstraints.CENTER;
		c.weighty = 0.0;
		c.weightx = 0.0;
		jpane.add(labelSimulation, c);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridy = 9; 
		c.gridx = 0;
		c.gridwidth =0;
		c.weighty = 1.0;
		jpane.add(scrollPaneSimulation,c);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridy = 10; 
		c.gridx = 0;
		c.gridwidth =0;
		c.weighty = 0.01;		
		jpane.add(textArea,c);
		
		//Se agrega timer
		lbl_time = new JLabel();
		lbl_time.setText("Tiempo restante de simulacion: " + World.getInstance().getTime());
		seconds = 0;
		c.anchor = GridBagConstraints.SOUTH;
		c.gridy = 11;
		jpane.add(lbl_time,c);
		
		initTimer();
			
		splitPane.setRightComponent(jpane);
		
		
		
		
		// Dibujador del mapa
		EntityDrawer entityDrawer = new EntityDrawer(gameMap);
		this.entityDrawer = entityDrawer;

		// Eventos mapa
		// Eventos mapa
		mapMouseHandler2 = new MapMouseHandler(gameMap);
		MapKeyHandler mapKeyHandler = new MapKeyHandler(gameMap, entityDrawer);
		
		mapMouseHandler2.setMapClickCommand(new EntityClicker(this, entityDrawer));
		jbuttonSender.addActionListener(new EntitySender(this, entityDrawer));
		
		this.gameMap.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.gameMap.setLayout(new BorderLayout(0, 0));
		
		setContentPane(splitPane);
		
		messageMediator =  MessageMediator.getInstance();
		messageMediator.setGameframe(this);
		
		printHelp();
		
		
	}

	/**
	 * Dibuja las entidades dadas en el mapa.
	 * @param entities entidades a dibujar.
	 */
	public void DrawEntities(List<? extends GameEntity> entities) {
		entityDrawer.DrawEntities(entities);
	}
	
	/**
	 * Imprime un mensaje en el panel de la ventana.
	 * @param message mensaje que se imprimir�.
	 */
	public void PrintMessage(String message) {
		messagePane.append(message + System.getProperty("line.separator"));
	}
	
	/**
	 * Imprime un mensaje en el panel de informaci�n de clicks
	 * @param message mensaje que se imprimir�.
	 */
	public void PrintMessageClick(String message) {		
		messagePaneClick.setText(message);				
	}
	
	public void setFinalSummary(FinalSummary finalsum) {		
		fs=finalsum;
		fs.gameFrame(this);
	}
	
	
		
	private void initTimer() {
		
		maxTime = World.getInstance().getTime();
		timer = new Timer(1000, new ActionListener(){     
            public void actionPerformed(ActionEvent e) {
               maxTime--;
               lbl_time.setText("Tiempo restante de simulacion: " + maxTime);
               if(maxTime == 0){
            	   World.getInstance().finishSimulation();
            	   timer.stop();  
            	   creator.Stop();
            	   jbuttonCreator.setEnabled(false);
            	   jbuttonSender.setEnabled(false);
            	   fs = new FinalSummary();  
            	   setFinalSummary(fs);
            	   fs.setVisible(true);
            	   
            	   
            	  
               }
            }
        });
		timer.start();
				
	}
	/**
	 * Imprime un mensaje en el panel de informaci�n de la simulaci�n.
	 * @param message mensaje que se imprimir�.
	 */
	public void PrintMessageSimulation(String message) {
		messagePaneSimulation.append(message + System.getProperty("line.separator"));
	}
	
	/**
	 * Muestra ayuda al usuario.
	 */
	private void printHelp() {
		PrintMessage("Ayuda: ");
		PrintMessage("WASD - movimiento");
		PrintMessage("Q/E - alejar/acercar");
		PrintMessage("Click - informacion");
	}
	
	/**
	 * Inicia un thread que renderea el mapa.
	 */
	private void initiateRendering() {
		Thread t = new Thread(new MapRenderer(this));
		t.start();
	}

	public void Close() {
		this.dispose();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		ClickLogger clickLogger = new ClickLogger(this, entityDrawer);
		mapMouseHandler2.setMapClickCommand(clickLogger);
		
	}
	
	
	
	public GameMap getGameMap() {
		return gameMap;
	}
	
	public MapClickCommand getMapClickCommand() {
		return mapMouseHandler2.getMapClickCommand();
	}
	
	public void setMapClickCommand(MapClickCommand mapClickCommand) {
		mapMouseHandler2.setMapClickCommand(mapClickCommand);
	}
	
	public IpPort getSelectedAddress() {
		return (IpPort) addressComboBox.getSelectedItem();
	}
	
	public void addNewEntity(Point point){
			
			int[] dimensions = new int[2];
			dimensions[0] = 3;
			dimensions[1] = 3;
			int civ = 0;
			
			String typeCiv  = (String) jComboBox1.getSelectedItem();
			
			if(typeCiv == "Mongol"){
				civ = 0;	
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}		
			else if(typeCiv == "Roman"){
				civ = 1;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);	
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Greek"){
				civ = 2;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Persian"){
				civ = 3;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Egipcian"){
				civ = 4;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Viking"){
				civ = 5;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Chinese"){
				civ = 6;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Japanese"){
				civ = 7;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Goth"){
				civ = 8;
				World.getInstance().addOneCivilization(point, dimensions, 5, civ);
				World.getInstance().sumOneToCivCreated();
			}
			else if(typeCiv == "Food"){
				World.getInstance().addOneResource(point, 0);
				World.getInstance().sumOneToResCreated();
			}
			else if(typeCiv == "River"){
				World.getInstance().addOneResource(point, 1);
				World.getInstance().sumOneToResCreated();
			}
			
			messageMediator.setSimulation("Se ha formado una civilizacion de " + jComboBox1.getSelectedItem() + " en el punto: (" + point.x + "," + point.y + ")");
			mapMouseHandler2.setMapClickCommand(new EntityClicker(this, entityDrawer));
			
			
			gameMap.setFocusable(true);
			gameMap.requestFocusInWindow();
		}
	

	
}
