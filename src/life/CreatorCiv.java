package life;

import java.util.Random;

public class CreatorCiv {

	private Random r;
	private World w;
	private MessageMediator messageMediator;
	private Thread t;
	
	
	public CreatorCiv(){
		
		t = new Thread( new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
					    Thread.sleep(5000);
					} catch (InterruptedException e) {
					    e.printStackTrace();					    
					}				
					Create();
				}
			}
		});
		
		t.start();
		
	}
	
		
	private void Create(){
		
		r = new Random();
		w = World.getInstance();
		messageMediator = MessageMediator.getInstance();
		double prob = r.nextDouble();
		
		
		if(prob <= 0.3){
			w.CreateOneRandomCivilizations();
			w.sumOneToAppearences();
			messageMediator.setSimulation("Atencion, por las condiciones climaticas, ha aparecido una nueva civilizacion");
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void Stop(){
		t.stop();
	}
	
	

}
