package life;

import java.awt.Point;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Random;

import javax.crypto.spec.PSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import life.civilization.Civilization;
import life.resource.Resource;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLSerializer {

	private Random r;
	private MessageMediator messageMediator;
	
	public XMLSerializer(){
		r = new Random();
		messageMediator = MessageMediator.getInstance();
	}
	
	
	public String Serialize(GameEntity gameEntity)
	{
		//Si se trata de una civilizacion se serializa con los parametros de dicha clase
		int x;
		int y;
		int Width;
		int Height;	
		String type = ""; 
		int population = 0;
		String XMLGenerado = "";
		String originalGroupID;
		String otherWorldInfo;

		x = gameEntity.getPosition().x;
		y = gameEntity.getPosition().y;
		Width = gameEntity.getWidth_height()[0];
		Height = gameEntity.getWidth_height()[1];
		originalGroupID = gameEntity.getOriginalGroupID() != null
						  ? gameEntity.getOriginalGroupID()
						  : "5";
		otherWorldInfo = gameEntity.getOtherWorldInfo() != null
						 ? gameEntity.getOtherWorldInfo()
						 : "";
		
		if(gameEntity instanceof Civilization){
			
			type = ((Civilization)gameEntity).getType().name();
			population = ((Civilization)gameEntity).getPopulation(); 
			messageMediator.setSimulation("Se ha enviado una Civilización de tipo " + type + " a otro mundo");
			
		}
		else if(gameEntity instanceof Resource){
			type = ((Resource)gameEntity).getType().name();
			population = 0;
			messageMediator.setSimulation("Se ha enviado un recurso de tipo " + type+ " a otro mundo");
		}
		
		XMLGenerado = "<GameOfLife><Common><PosX>"+x+"</PosX><PosY>"+y+"</PosY><Width>"+Width+"</Width><Height>"+Height+"</Height>"
				+ "<OriginalGroupID>"+originalGroupID+"</OriginalGroupID></Common><WorldSpecific><World id=\"5\"><Type>"+type+"</Type>"
				+ "<Population>"+population+"</Population></World>"+otherWorldInfo+"</WorldSpecific></GameOfLife>";
		
		
		return XMLGenerado;
	}
	
	public void Deserialize(String xmlRecords)
	{

	   DocumentBuilder db = null;
	   
	   //Informacion de la entidad
	   int numPosX = 0;
	   int numPosY = 0;
	   int numWidth = 0;
	   int numHeight = 0;
	   int groupId = 0;
	   NodeList name;
	   NodeList nodes;
	   
	try {
		db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	} catch (ParserConfigurationException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	   InputSource is = new InputSource();
	   is.setCharacterStream(new StringReader(xmlRecords));

		Document doc = null;
		try {
			doc = db.parse(is);
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		nodes = doc.getElementsByTagName("Common");
		String[] informationCommon = {"PosX","PosY","Width","Height","OriginalGroupID"};
			    
	    for (int i = 0; i < nodes.getLength(); i++) {
	      Element element = (Element) nodes.item(i);
	      
	      for(int j=0; j<informationCommon.length; j++){
	    	  
		      name = element.getElementsByTagName(informationCommon[j]);
		      Element line = (Element) name.item(0);
		      
		      //System.out.println(informationCommon[j]+": " + getCharacterDataFromElement(line));
		      messageMediator.setSimulation(informationCommon[j]+": " + getCharacterDataFromElement(line));
		      switch(j){
		      case 0: numPosX = Integer.parseInt(getCharacterDataFromElement(line)); break;
		      case 1: numPosY = Integer.parseInt(getCharacterDataFromElement(line)); break;
		      case 2: numWidth = Integer.parseInt(getCharacterDataFromElement(line)); break;
		      case 3: numHeight = Integer.parseInt(getCharacterDataFromElement(line)); break;
		      case 4: groupId = Integer.parseInt(getCharacterDataFromElement(line)); break;
		      
		      }
		      }
	    }
	    
	    System.out.println(nodeToString(doc));
	    
	      //Obtener informacion especifica
	    int civ = -1; //Civilizacion que se creara (-1 si se trata de una entidad de otro mundo
	    String civType = "DEFAULT"; //Default en caso de ser una entidad de otro mundo
	    int population = 0;
	     String[] informationCivilization= {"Type","Population"};
	   
	      nodes = doc.getElementsByTagName("World");
	      
	      String otherWorldInfo = "";
	      /*
	       * Se analiza la informacion especifica de la entidad solamente si se trata de
	       * una entidad de nuestro mundo
	       */
	      
	      //if(groupId==5){
	      for (int i = 0; i < nodes.getLength(); i++) {     
		    
		      Element world = (Element) nodes.item(i);

		      if (!world.getAttribute("id").equals("5")) {
		    	  otherWorldInfo += nodeToString(nodes.item(i));
		    	  continue;
		      }
		      
		      for(int j=0; j<informationCivilization.length;j++){
		    	 
		    	  name = world.getElementsByTagName(informationCivilization[j]);
		    	  Element info = (Element) name.item(0);
		    	  //System.out.println(informationCivilization[j]+": " + getCharacterDataFromElement(info));
		    	  messageMediator.setSimulation(informationCivilization[j]+": " + getCharacterDataFromElement(info));
		    	  switch(j){
		    	  case 0: civType = (getCharacterDataFromElement(info)); break;
		    	  case 1: population = Integer.parseInt(getCharacterDataFromElement(info)); break;
		    	  }
		      }
	      }
	      //}
	      
	      
	      	//Creacion de la entidad
	            int randomX = r.nextInt(10)-5;
	            int randomY = r.nextInt(10)-5;
	      		Point position = new Point(numPosX+randomX,numPosY+randomY);
	      		int[] dimensions  = {numWidth,numHeight};
	      		civ = DetCivType(civType);
	      		messageMediator.setSimulation("Se ha recibido una nueva entidad y se ha puesto en el posición (" + position.x + ","+ position.y +")");
	      		String originalGroupID = doc.getElementsByTagName("OriginalGroupID").item(0).getTextContent();
	      		/*
	      		 * Creacion de civilizaciones de otros mundos, de este mundo y recursos.
	      		 * Dependera del valor de civ
	      		 */
	      		
	      		if(civ == -1){ 
	      			
	      			civ = DetCivOtherWorld(dimensions);
	      			World.getInstance().addOneCivilization(position,dimensions, population, civ,
	      					originalGroupID, otherWorldInfo);
	      		}
	      		else if(civ<=8 && civ>=0){
	      		World.getInstance().addOneCivilizationMigrate(position,dimensions, population, civ,
	      				originalGroupID, otherWorldInfo);
	      		messageMediator.setSimulation("Se ha recibido una nueva entidad y se ha puesto en el posición (" + position.x + ","+ position.y +")");
	      		}
	      			
	      		//Creacion de recurso
	      		else{
	      			messageMediator.setSimulation("Se ha recibido una nueva entidad y se ha puesto en el posición (" + position.x + ","+ position.y +")");
	      			int res = 0;
	      			switch(civ){
	      			case 9: res = 0; break;
	      			case 10: res = 1; break;
	      			}
	      			World.getInstance().addOneResource(position, res, originalGroupID, otherWorldInfo);
	      			World.getInstance().sumOneToReceivedRes();
	      		}
	      		
	}
	    	

	private String nodeToString(Node node) {
		StringWriter sw = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "no");
			t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
        	System.out.println("nodeToString Transformer Exception");
		}
		return sw.toString();
	}
	
	public static String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	      CharacterData cd = (CharacterData) child;
	      return cd.getData();
	    }
	    return "";
	  }
	  
	  //Hace el cambio de string a int, para no tener que cambiar los metodos de World
	private int DetCivType(String type){
		  int typeCiv = -1;
		  switch(type){
		  case "MONGOL": typeCiv = 0; break;
		  case "ROMAN": typeCiv = 1; break;
		  case "GREEK": typeCiv = 2; break;
		  case "PERSIAN": typeCiv = 3; break;
		  case "EGYPCIAN": typeCiv = 4; break;
		  case "VIKING": typeCiv = 5; break;
		  case "CHINESE": typeCiv = 6; break;
		  case "JAPANESE": typeCiv = 7; break;
		  case "GOTH": typeCiv = 8; break;
		  case "FOOD": typeCiv = 9; break;
		  case "RIVER": typeCiv = 10; break;
		  }
		  return typeCiv;
	  }
	  
	/*
	 * Dependiendo del rango entre el que se encuentran sus dimensiones, se crea una diferente
	 * civilizacion. Este metodo es utilizado para entidades provenientes de otros mundos.
	 * Recibe el arreglo de dimensiones y lo modifica para ajustarse a los de una civilizacion
	 * de nuestro juego. Utiliza una funcion modulo, de forma tal que dependiendo de las dimensiones pueda
	 * ser cualquier tipo de civilizacion
	 */
	private int DetCivOtherWorld(int[] dimensions){
		
		int x = dimensions[0];
		int y = dimensions[1];		
		int civType = (x+y)%9;
		
		return civType;
	}
	
	}
	


