package life;

import life.graphic.GameFrame;

public class MessageMediator {
	
	private static MessageMediator messageMediator = null;
	private GameFrame gameFrame;
	//private String message;

	private MessageMediator(){
		
		
	}
	
	public static MessageMediator getInstance(){
		if(messageMediator == null){
			messageMediator = new MessageMediator();
		}
		return messageMediator;
	}
	
	public void setGameframe(GameFrame gameFrame){
		this.gameFrame = gameFrame;
	}
	
	public void setSimulation(String message){
		gameFrame.PrintMessageSimulation(message);
		
	}
}
