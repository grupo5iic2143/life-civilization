# Life-civilization

Life-civilization es un juego educativo que simula el comportamiento de
microorganismos represent�ndolos como civilizaciones que crecen, combaten y se
conquistan.

## C�mo jugar

Al iniciar el juego, deber� especificar las condiciones iniciales de la
simulaci�n. Estas determinar�n el tipo de civilizaciones que aparecer�n durante
el juego.

Luego de esto comenzar� la simulaci�n y podr� ver a las civilizaciones
interactuar entre ellas.

Si desea saber m�s sobre el juego y sus reglas, puede revisar el manual en
`docs/Life Civilization Manual.pdf`.

### Controles

Para mover la c�mara, utilice las teclas WASD. Para acercar y alejar la c�mara,
utilice Q y E.

Para agregar nuevas civilizaciones o enviarlas a otros mundos utilice los
botones de la derecha y luego haga click en el lugar donde desea agregar la
civilizaci�n o en la civilizaci�n que desea enviar.

## Configuraci�n

Puede configurar las direcciones IP y los puertos de los otros mundos en el
archivo `addresses.properties`.

## Building

Para compilar y correr el juego, puede ejecutar el comando `ant run`.

## Autores

* Esteban Dib
* Pablo Espinosa
* Francisco Maturana
* Iv�n Rubio