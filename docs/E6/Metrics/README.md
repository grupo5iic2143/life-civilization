# An�lisis Metrics

## General

![Metrics general](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Metrics/MetricsGeneral.png)

Podemos ver que, en promedio, todos los indicadores tienen valores razonables.
Sin embargo, hay algunos casos que tienen valores extremos, espec�ficamente
en los indicadores "Method Lines of Code" y "McCabe Cyclomatic Complexity".
Estos casos se discuten con mayor detalle m�s adelante.

## GameFrame

![Metrics GameFrame](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Metrics/MetricsGameFrame.png)

En primer lugar, podemos ver que la clase `GameFrame` posee un m�todo con 127
l�neas de c�digo, lo que es bastante largo. Esto corresponde al constructor
de la clase, que coloca y configura todos los elementos de la interfaz. Este
c�digo, sin embargo, no fue escrito en su totalidad, sino que fue generado a
trav�s del plugin "WindowBuilder", lo que hace que a pesar de su longitud, el
m�todo sea f�cil de mantener.

En segundo lugar, la complejidad ciclom�tica del m�todo `addNewEntity` es alta
ya que se realiza un `switch` sobre la enumeraci�n `life.civilization.Type`,
caso que se repite en otras partes del proyecto, pero que consideramos que era
pr�cticamente inevitable y que no ten�a un efecto negativo dado que los cuerpos
de cada `case` son simples.

## Move

![Metrics Move](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Metrics/MetricsMove.png)

En esta clase no destaca ning�n problema, si bien algunas acciones podr�an ser
extra�das a m�todos externos para hacer el m�todo principal m�s corto.

## World

![Metrics World](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Metrics/MetricsWorld.png)

En esta clase lo que m�s llama la atenci�n es la complejidad ciclom�tica.
Podemos ver cuales son los m�todos que presentan este problema.

![Metrics World Specific](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Metrics/MetricsWorldSpecific.png)

Lo que tienen en com�n estos m�todos es, nuevamente, que hacen un `switch`
sobre los distintos tipos de civilizaci�n, lo que inmediatamente eleva esta
m�trica.