# An�lisis JProfiler

## Memoria

Al iniciar la aplicaci�n, tenemos el siguiente uso de memoria.

![Memoria Inicio](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Jprofiler/MemoriaInicio.png)

Podemos ver que mayor�a de los objetos en memoria corresponden a `Strings` o
similares.

Una vez que comenzamos a jugar el uso de la memoria cambia y despu�s de un rato
se ve as�.

![Memoria Jugando](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Jprofiler/MemoriaJugando.png)

A pesar de que que seguimos teniendo una gran cantidad de `Strings`, ahora
podemos ver tambi�n varios objetos que corresponden a la sincronizaci�n, como
los `ReentrantLock` y varios que corresponden a la interfaz, como `Rectangle2D`
y `Line2D`.

## CPU

A continuaci�n podemos c�mo fue el uso de la CPU a trav�s de una sesi�n de
juego.

![CPU Gr�fico](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Jprofiler/CPUGrafico.png)

Como se muestra en la imagen, hay dos momentos en que vemos un cambio notorio
en el uso de la CPU: cuando iniciamos la aplicaci�n y cuando comenzamos a
jugar. Este comportamiento es parte de lo esperable.

La siguiente imagen muestra la cantidad de tiempo que se ejecuta cada m�todo.

![CPU Jugando](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Jprofiler/CPUJugando.png)

Podemos ver que la mayor�a del tiempo se utiliza en el m�todo que ejecuta la
interfaz de usuario, que tiene sentido, ya que esta se ejecuta todo el tiempo
mientras que las dem�s cosas se ejecutan peri�dicamente.

## Threads

A continuaci�n se muestra c�mo se uso el tiempo que los threads se mantuvieron
activos.

![Threads Jugando](https://bitbucket.org/grupo5iic2143/life-civilization/raw/master/docs/E6/Jprofiler/ThreadsJugando.png)

Podemos apreciar que se mantuvieron la mayor�a del tiempo esperando y, en
general, poco tiempo ejecut�ndose o bloqueados. Esto se debe a que deb�an
ejecutar tareas relativamente simples y cortas.